<!DOCTYPE html>
<html lang="pt-br">
	<head>

		<!-- META -->
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta property="og:title" content="" />
		<meta property="og:description" content="" />
		<meta property="og:url" content="" />
		<meta property="og:image" content=""/>
		<meta property="og:type" content="website" />
		<meta property="og:site_name" content="" />

		<!-- TÍTULO -->
		<title>Processos Pier do Victor</title>

		<!-- FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Hammersmith+One|Rubik:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
		<link rel="stylesheet" href="css/site.css" />

		<!-- JS -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/geral.js"></script>

		<!-- FAVICON -->
		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" /> 

	</head>
	<body>
	<div class="telaAutenticacao">
		<div class="areaLogin">
			<a href="http://restaurantesvictor.com.br/" target="_blank"><img src="img/logo.png" alt=""></a>

			<p>Autenticação Processos <i class="fa fa-lock" aria-hidden="true"></i></p>
			<input type="text" placeholder="Login*" id="login">
			<input type="password" placeholder="Senha*" id="senha">
			<button class="entrar">Entrar</button>
			<span class="alert">Preencha os campos corretamente*</span>
		</div>
	</div>
	<div class="bannerTopo" style="background:url(img/fundo.JPG)">
		<div class="areaTitulo">
			<a href="http://restaurantesvictor.com.br/" target="_blank"><img src="img/logo.png" alt=""></a>
			<p>Processos Restaurantes Victor</p>
		</div>
	
	</div>
	<div class="container">
	
		<h4>Processos</h4>
		<div class="areaLinks">

			<section>
				<button class="btn"><i class="fa fa-plus-square-o" aria-hidden="true"></i> GMT e TI (Manutenção e TI)</button>
				<div class="area" style="display:none">
					
					<span>GMT e TI (Manutenção e TI)</span>
					<p>
						<a target="_blank" href="http://app.pipefy.com/public_form/154699">Abrir Ordem de Serviço Manutenção</a>
					</p>
					<p>
						<a target="_blank" href="http://app.pipefy.com/public_form/218833">Abrir Ordem de Serviço TI</a>
					</p>
				</div>
			</section>

			<section>
				<button class="btn"><i class="fa fa-plus-square-o" aria-hidden="true"></i> GC (Compras)</button>
				<div class="area" style="display:none">
					
					<span>GC (Compras)</span>
					<p>
						<a target="_blank" href="http://app.pipefy.com/public_form/158371">Compra de Utensílios</a>
					</p>
				</div>
			</section>

			<section>
				<button class="btn"><i class="fa fa-plus-square-o" aria-hidden="true"></i> GP (RH)</button>
				<div class="area" style="display:none">
					
					<span>GP (RH)</span>
					<p>
						<a target="_blank" href="http://app.pipefy.com/public_form/154455">Abrir Vaga (Contratação)</a>
					</p>
					<p>
						<a target="_blank" href="http://app.pipefy.com/public_form/164167">Solicitar Desligamento</a>
					</p>
					<p>
						<a target="_blank" href="http://app.pipefy.com/public_form/164202">Solicitar Alteração de Função</a>
					</p>
					<p>
						<a target="_blank" href="http://app.pipefy.com/public_form/164226">Solicitar Alteração de Salário</a>
					</p>
					<p>
						<a target="_blank" href="http://app.pipefy.com/public_form/164257">Agendar Férias</a>
					</p>
					<p>
						<a target="_blank" href="http://app.pipefy.com/public_form/164194">Solicitar Treinamento</a>
					</p>

				</div>
			</section>	

			<section>
				<button class="btn"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Operação</button>
				<div class="area" style="display:none">
					
					<span>Operação</span>
					<p>
						<a target="_blank" href="http://app.pipefy.com/public_form/97985">Check List Abertura de Turno</a>
					</p>
					<p>
						<a target="_blank" href="http://app.pipefy.com/public_form/129089">Check List Líder Salão</a>
					</p>
					<p><a target="_blank" href="https://app.pipefy.com/public_form/158054">Check List Fechamento BVSL </a><p>
					<p><a target="_blank" href="https://app.pipefy.com/public_form/129277">Check List Fechamento BVPE </a><p>
					<p><a target="_blank" href="https://app.pipefy.com/public_form/160563">Check List Fechamento BISTRO </a><p>

				</div>
			</section>

			<section>
				<button class="btn"><i class="fa fa-plus-square-o" aria-hidden="true"></i> GQ/P (Nutricionista)</button>
				<div class="area" style="display:none">
					
					<span>GQ/P (Nutricionista)</span>
					<p>
						<a target="_blank" href="http://app.pipefy.com/public_form/209610">Segurança Alimentar</a>
					</p>
				</div>
			</section>

		
			
		</div>
</div>
	</body>

	<script>
		$(function(){

			$(".entrar").click(function(e){
				var login = $("#login").val();
				var senha =  $("#senha").val();
				$.ajax({
					url: "validacao.php",
					type: 'GET',
					data : {
						login    : login,
						senha      : senha,
					},
					dataType: 'json',
					cache: false,
					success: function (resp) {

						 	if (resp.resposta == true){
						 		$(".telaAutenticacao").slideUp();
						 		$('.btn').removeAttr('disabled');
						 	}else{
						 		$(".alert").show();
						 	
						 	}
					},
					error: function(resp) {

						alert('erro');
					}

				});
			});
		});
	</script>
</html>


