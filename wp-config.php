<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'projetos_restaurantesvictor_blog');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY', 'MuT$57_ o6UZnC:+][TYQwW.IRFheAeKg=F|+7D7m^aS26U;awqF=2_j7$~T-Sr_' );
define( 'SECURE_AUTH_KEY', 'HvQu%XfR4tUU%;.PVl7C:ZRe=XGK0b}[qu(rMWE8 f!c+!0x=LDq*p1Fh]Cas{Cl' );
define( 'LOGGED_IN_KEY', 'dpef#0kOaa/NM=_XZQOx(1-D~8&DHeDJ=lycqPbdyeNB}!x100k4WoWvvNXod~(#' );
define( 'NONCE_KEY', 'R!K?N,Y1hvQ3_Z,jaiZ`gYhJx1Y>Ik[uO]3<(Lc0WvLoAoy#~whK@rxsVJV8D*d@' );
define( 'AUTH_SALT', '1%UrUY[Ql>La0OjbL1yX%lx8YK2>;%`**jJ)QT8v1:*xT0yOa%^nlZ#s0XuuQF4Z' );
define( 'SECURE_AUTH_SALT', 'XX<R-K)MQhC,bF#!R:R`6;ADkAS)soptF7xzga@QS3R8(Lf#3myNHGD&M ?@`+[6' );
define( 'LOGGED_IN_SALT', 'jG2k|s] *soU4{/7.rj^Cs`U%(GVHAYY:NZl8Jkf#{UI}u&)W.TlPqj7ci6xw^^;' );
define( 'NONCE_SALT', '=iC;qrfG)16Eww729RR#PWO[LW`bZAkcpsItx)63I_Jmj8qNw%E;1&8{n)&<=v@/' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'rv_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

define( 'WP_MEMORY_LIMIT', '550M' );
/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
//Disable File Edits
define('DISALLOW_FILE_EDIT', true);