<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Restaurantes_do_Victor
 */

get_header(); ?>

	<div class="pg pg-blog">

		<div class="banner-blog" style="background:url(<?php bloginfo('template_directory'); ?>/img/foto-blog.png)">
			<div class="nome-blog">
				<p>últimas notícias  do blog</p>				
			</div>
		</div>

		<section class="container blog">
			
			<div class="row">
			<?php echo get_sidebar(); ?>

				<div class="col-md-9">
					<div class="posts-blog">
						
					<ul>
						<?php 
						
							
			            	
							if ( have_posts() ) : while ( have_posts() ) : the_post();
							$fotoBlog = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$fotoBlog = $fotoBlog[0];
							
								
								
						?>

					
						<li>
							<a href="<?php echo get_permalink(); ?>"  id="blog" alt="<?php echo get_the_title() ?>" class="blog-foto-inicial" style="background:url(<?php echo $fotoBlog ?>)">
								<div class="lente">
									<span class="link-post">ver mais</span>
								</div>
							</a>
							
							<a href="">
								<div class="blog-descricao-inicial hvr-shutter-out-vertical">
									<p><?php echo get_the_title() ?></p>
									<span><?php the_time('j \d\e F \d\e Y') ?></span>
									<small><?php customExcerpt(70); ?></small>
								</div>
							</a>
						</li>
							<?php endwhile; wp_reset_query(); ?>

               			<?php endif; ?>	
					</ul>


					</div>
				</div>
			
			</div>

		</section>

	</div>


<?php

get_footer();
 include (TEMPLATEPATH . '/inc/scriptMapa.php');