
$(function(){
 	
 	
	$( ".cardapioIn a" ).click(function(e) {
		event.preventDefault();
		$( ".loadCardapio" ).fadeIn(800);
		$( "#floatingCirclesG" ).show();
		setTimeout(function(){ 
			$( ".cardapioInicial" ).addClass('cardapioInicialSlideIn');
			$( ".cardapioInicial" ).removeClass('cardapioInicialSlideOut');
			$( ".cardapioInicial" ).show();
		}, 3000);
	});

	$( "#fecharInicial" ).click(function() {
		$( ".cardapioInicial" ).addClass('cardapioInicialSlideOut');
		$( ".cardapioInicial" ).removeClass('cardapioInicialSlideIn');
		$( "#floatingCirclesG" ).hide();
		setTimeout(function(){ 
			$( ".cardapioInicial" ).hide();
			$( ".loadCardapio" ).fadeOut(300);

		}, 1000);
	});


	$( ".cardapioInicial .sidebar aside nav h3" ).click(function() {
		$(".cardapioInicial .sidebar aside nav h3").removeClass('ativo');
		$(this).addClass('ativo');
		var slug = $(this).attr('data-slug');
		console.log(slug);
		$("."+slug).show();
		$("."+slug).addClass('mostrarCardapioSlideIn');
		$("."+slug+" .contentCardapioSidebar").addClass('SidebarFadeIn');

		
		 
	});

	$( ".cabecalhoCardapio" ).click(function() {
		$( ".efeitoFade .contentCardapioDescripition" ).addClass('descripitionFadeOut');
		$( ".efeitoFade .contentCardapioSidebar" ).addClass('SidebarFadeout');
		$( ".efeitoFade" ).addClass('CardapioPrincipalFadeOut');
		setTimeout(function(){ 
			$( ".efeitoFade" ).hide();
			$( ".efeitoFade .contentCardapioDescripition" ).removeClass('descripitionFadeOut');
			$( ".efeitoFade .contentCardapioSidebar" ).removeClass('SidebarFadeout');
			$( ".efeitoFade" ).removeClass('CardapioPrincipalFadeOut');
		}, 700);
	});

	$(".fecharPrincial").click(function(e) {
		$( ".efeitoFade .contentCardapioDescripition" ).addClass('descripitionFadeOut');
		$( ".efeitoFade .contentCardapioSidebar" ).addClass('SidebarFadeout');
		$( ".efeitoFade" ).addClass('CardapioPrincipalFadeOut');
		setTimeout(function(){ 
			$( ".efeitoFade" ).hide();
			$( ".efeitoFade .contentCardapioDescripition" ).removeClass('descripitionFadeOut');
			$( ".efeitoFade .contentCardapioSidebar" ).removeClass('SidebarFadeout');
			$( ".efeitoFade" ).removeClass('CardapioPrincipalFadeOut');
		}, 700);
	});


	$('a.scroll').click(function() {
		$("#fecharPrincial").hide();
		setTimeout(function(){ 
			$("#fecharPrincial").show(200);
		}, 1000);

		$(".mostrarCardapio .contentCardapioSidebar .itemSidebar h2").removeClass('ativo');
		$(this).children('h2').addClass('ativo');
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}
		}
		

	});

	$('a.cabecalhoCardapio').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}
		}
		
	});

	$("a#fancy").fancybox({
		'titleShow' : false,
		openEffect	: 'elastic',
		closeEffect	: 'elastic',
		closeBtn    : true,
		arrows      : true,
		nextClick   : true
	});

});


