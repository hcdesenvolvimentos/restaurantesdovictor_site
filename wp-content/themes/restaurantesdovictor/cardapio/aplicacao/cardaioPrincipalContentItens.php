<section id="<?php echo $listaSubCategorias->slug ?>">
	
	<?php 
		// RECUPERANDO CATEGORIAS FILHAS/PASSAR PARAMETRO CAT_ID
		$subCategoriaCardapioFilhas = array(
			'taxonomy'     => 'categoriaCardapio',
			'child_of'     => 0,
			'parent'       => $listaSubCategorias->cat_ID,
			'orderby'      => 'name',
			'pad_counts'   => 0,
			'hierarchical' => 1,
			'title_li'     => '',
			'hide_empty'   => 0
		);	

		// FUNÇÃO RECUPERAR CATEGORIAS
		$listarSubCategoriaCardapioFilhas = get_categories($subCategoriaCardapioFilhas);

		foreach ($listarSubCategoriaCardapioFilhas as $listarSubCategoriaCardapioFilhas):
			$listarSubCategoriaCardapioFilhas = $listarSubCategoriaCardapioFilhas;
			$categoriaAtivaImg = z_taxonomy_image_url($listarSubCategoriaCardapioFilhas->term_id);
	?>
	<figure class="descripitionImgCardapio" style="background: url(<?php echo $categoriaAtivaImg ?>)"></figure>
	
	<div class="descripition">
		<div class="item">
			<h2><?php echo $listarSubCategoriaCardapioFilhas->name; ?></h2>
			<?php 

				//LOOP DE POST CATEGORIA DESTAQUE				
				$pratosCardapio = new WP_Query(array(
					'post_type'     => 'cardapio',
					'posts_per_page'   => -1,
					'tax_query'     => array(
						array(
							'taxonomy' => 'categoriaCardapio',
							'field'    => 'slug',
							'terms'    => $listarSubCategoriaCardapioFilhas->slug,
							)
						)
					)
				);

				// LOOP DE PRATOS
				while ( $pratosCardapio->have_posts() ) : $pratosCardapio->the_post();
		        $pratosFoto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
		        $pratosFoto = $pratosFoto[0];

		        	$cardapio_vegetariano = rwmb_meta('Restaurantesdovictor_cardapio_vegetariano');
		        	$cardapio_lactose = rwmb_meta('Restaurantesdovictor_cardapio_lactose');
		        	$prato_galeria = rwmb_meta('Restaurantesdovictor_prato_galeria');
		        	$cardapio_galeria = rwmb_meta('Restaurantesdovictor_cardapio_galeria');
		        	$prato_galeria = rwmb_meta('Restaurantesdovictor_prato_galeria');
		        	if ($cardapio_vegetariano == 1 && $cardapio_lactose == 1) {
		        		$vegetarianoLactose = "vegetarianoLactose";
		        	}elseif ($cardapio_vegetariano == 1) {
		        		$icone = "vegetariano";
		        	}elseif ($cardapio_lactose == 1) {
		        		$icone = "lactose";
		        	}else{
		        		$icone = "";	
		        	}
		        	
			?>
			<h3 class="<?php echo $vegetarianoLactose ?>">
				<?php if ($prato_galeria == 1): 

					foreach ($cardapio_galeria as $cardapio_galeria):
						$cardapio_galeria = $cardapio_galeria['full_url'];
					
				?>

					<a href="<?php echo $cardapio_galeria ?>" id="fancy" rel="gallery1"></a>
				<?php endforeach;endif; ?>
				<?php echo get_the_title(); ?> 
				<small class="<?php echo $icone ?> "></small>
			</h3>
			<?php echo the_content(); ?>
			<?php endwhile; wp_reset_query();  ?>
		</div>
		
	</div>
	<?php endforeach; ?>
	
</section>