
<?php 
	// RECUPERANDO CATEGORIAS PAI
	$categoriaCardapio = array(
		'taxonomy'     => 'categoriaCardapio',
		'child_of'     => 0,
		'parent'       => 0,
		'orderby'      => 'name',
		'pad_counts'   => 0,
		'hierarchical' => 1,
		'title_li'     => '',
		'hide_empty'   => 0
	);

	// FUNÇÃO RECUPERAR CATEGORIAS
	$listaCategorias = get_categories($categoriaCardapio);
	
	// SE TIVER CATEGORIAS
	if ($categoriaCardapio):

	// LOOP DE CATEGORIAS PAI
	foreach ($listaCategorias  as $listaCategorias):
		$listaCategoria = $listaCategorias;

		// IMAGEM CATEGORIA
		$categoriaAtivaImg = z_taxonomy_image_url($listaCategoria->term_id);

		// RECUPERANDO CATEGORIAS FILHAS/PASSAR PARAMETRO CAT_ID
		$subCategoriaCardapio = array(
			'taxonomy'     => 'categoriaCardapio',
			'child_of'     => 0,
			'parent'       => $listaCategorias->cat_ID,
			'orderby'      => 'name',
			'pad_counts'   => 0,
			'hierarchical' => 1,
			'title_li'     => '',
			'hide_empty'   => 0
		);	
		if ($subCategoriaCardapio):
?>	

<!-- CARDPÁIO / PASSANDO SLUG CATEGOTIA - PARA CHAMAR CARDÁPIO -->
<section class="cardapioPrincipal <?php echo $listaCategoria->slug ?>  mostrarCardapio efeitoFade" style="background: url(<?php echo get_template_directory_uri() ?>/cardapio/img/fotoHome.jpg);">

	<!-- FECHAR CARDÁPIO -->
	<button class="fecharCardapio fecharPrincial" id="">X</button>

	<!-- CABEÇALHO -->
	<div class="cabecalhoCardapio">
		<!-- LOGO -->
		<img src="<?php echo get_template_directory_uri() ?>/cardapio/img/logoCardapio.png" alt="" class="logo">
		<!-- BTN VOLTAR -->
		<button class="voltar">< Voltar</button>
		<!-- CATEGORIA NOME -->
		<h2><?php echo $listaCategoria->name ?></h2>
	</div>

	<!-- CONTENT CONTEÚDO -->
	<div class="contentCardapio">
		
		<!-- SIDEBAR  CARDÁPIO-->
		<?php include (TEMPLATEPATH . '/cardapio/aplicacao/cardaioPrincipalSidebar.php'); ?>
		
		<!-- LOOP DE PRATOS CARDÁPIO -->
		<?php include (TEMPLATEPATH . '/cardapio/aplicacao/cardaioPrincipalContent.php'); ?>
	
	</div>

</section>
<?php endif;endforeach;endif; ?>