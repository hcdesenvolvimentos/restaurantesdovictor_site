<?php
session_start();
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Restaurantes_do_Victor
 */
global $configuracao;

if ($_SESSION['nomeFranquia'] == "Bar do Victor") {	
	
	$logoFraquias = $configuracao['opt-logo']['url'];
	$titulo = $configuracao['opt-frase-titulo'];
	$endereco = $configuracao['opt-endereco'];
	$frase = $configuracao['opt-frase'];
	$iconMapa = $configuracao['opt-icon-mapa']['url'];
	$formularioContato = $configuracao['opt-formulario-mapa'];
	$horario = $configuracao['opt-horarioF'];

} else if ($_SESSION['nomeFranquia'] == "Bistrô do Victor") {


	$logoFraquias = $configuracao['opt-logo-bistro']['url'];
	$titulo = $configuracao['opt-frase-titulo-bistro'];
	$endereco = $configuracao['opt-endereco-bistro'];
	$frase = $configuracao['opt-frase-bistro'];
	$iconMapa = $configuracao['opt-icon-mapa-bistro']['url'];
	$formularioContato = $configuracao['opt-formulario-bistro'];
	$horario = $configuracao['opt-horarioB'];

} else if ($_SESSION['nomeFranquia'] == "Petiscaria do Victor") {
	
	$logoFraquias = $configuracao['opt-logo-Petiscaria']['url'];
	$titulo = $configuracao['opt-frase-titulo-Petiscaria'];
	$endereco = $configuracao['opt-endereco-Petiscaria'];
	$frase = $configuracao['opt-frase-Petiscaria'];
	$iconMapa = $configuracao['opt-icon-mapa-Petiscaria']['url'];
	$formularioContato = $configuracao['opt-formulario-Petiscaria'];
	$horario = $configuracao['opt-horarioP'];
	

} else if ($_SESSION['nomeFranquia'] == "Bar do Victor &#8211; Praça da Espanha") {
	
	$logoFraquias = $configuracao['opt-logo-espanha']['url'];
	$titulo = $configuracao['opt-frase-titulo-espanha'];
	$endereco = $configuracao['opt-endereco-espanha'];
	$frase = $configuracao['opt-frase-espanha'];
	$iconMapa = $configuracao['opt-icon-mapa-espanha']['url'];
	$formularioContato = $configuracao['opt-formulario-espanha'];
	$horario = $configuracao['opt-horarioS'];

}else{
	$logoFraquias = $configuracao['opt-logo-restaurantes']['url'];
	$titulo = $configuracao['opt-frase-titulo-restaurantes'];
	
	$iconMapa = $configuracao['opt-icon-mapa-restaurantes']['url'];
	$formularioContato = $configuracao['opt-formulario-restaurantes'];

	// ENDEREÇO DOS RESTAURANTES 
	$endereco = " ";
	$frase = $configuracao['opt-frase-restaurantes'];

	// BAR DO VICTOR PRAÇA DA ESPANHA 
	$enderecoE = $configuracao['opt-endereco-espanha'];
	$iconMapaE = $configuracao['opt-icon-mapa-espanha']['url'];

	//PETISCARIA
	$enderecoP = $configuracao['opt-endereco-Petiscaria'];	
	$iconMapaP = $configuracao['opt-icon-mapa-Petiscaria']['url'];

	// BISTRÔ
	$enderecoB = $configuracao['opt-endereco-bistro'];	
	$iconMapaB = $configuracao['opt-icon-mapa-bistro']['url'];

	//BAR DO VICTOR SÃO LOURENÇO 
	$enderecoS = $configuracao['opt-endereco'];
	$iconMapaS = $configuracao['opt-icon-mapa']['url'];
		
}

$enderecoGoogle = 'https://www.google.com.br/maps/place/' . urldecode($endereco);

?>


	<!-- RODAPÉ -->
	<footer class="rodape">


			<?php if ($endereco == " "): ?>	

		
			<div class="mapa" style="">
				<div class="areaMapa" id="map"></div>
				<div class="areaBtnMapa">
					<a href="<?php echo $enderecoGoogle; ?>"  target="_blank" title="Detecta no Google Maps" class="btnMapa"></a>
				</div>
				
			</div>
		

	
			<?php else: ?>	
			<a href="<?php echo $enderecoGoogle; ?>" target="_blank"> 
				<div class="mapa" style="">
					<div class="areaMapa" id="map"></div>
					<div class="areaBtnMapa">
						<a href="<?php echo $enderecoGoogle; ?>" target="_blank" title="Detecta no Google Maps" class="btnMapa"></a>
					</div>

				</div>
			</a>
			<?php endif; ?>		
		

		<div class="mapa" style="display:none">
			<iframe src="https://maps.google.com.br/maps?q=<?php echo  $endereco ?>&output=embed"  frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>

		<div class="copyright">			
			<p><i class="fa fa-copyright" aria-hidden="true"></i> Copyryght 2017 - Restaurantes Victor -Todos os Direitos reservados</p>
		</div>
		<div class="col-md-12" style="padding: 20px;text-align: center; background: #222528;">
			<small>
				<span style="font-size: 16px;color:#ccc;">Desenvolvido por</span>
				<a href="http://palupa.com.br/" target="_blank" title="Desenvolvido por Palupa Marketing" style="display: inline-block;"><img src="<?php bloginfo('template_directory'); ?>/img/palupaLogo.png" style="max-height: 15px;"></a>
			</small>
		</div>
	</footer>






</body>
</html>
