<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Restaurantes_do_Victor
 */
$fotoBlog = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$fotoBlog = $fotoBlog[0];


get_header(); ?>

<div class="pg pg-postagem">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.7&appId=1544579195844789";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	<div class="banner-blog" style="background:url(<?php bloginfo('template_directory'); ?>/img/foto-blog.png)">
		<div class="nome-blog">
			<p>últimas notícias  do blog</p>				
		</div>
	</div>

	<section class="container blog">
		
		<div class="row">
			<?php echo get_sidebar();  ?>

			<div class="col-md-9">
				<div class="area-postagem-blog">
					<div class="foto-postagem" style="background:url(<?php echo $fotoBlog  ?>)"></div>
					<div class="titulo-postagem">
						<h2><?php echo get_the_title() ?></h2>
						<span><?php the_time('j \d\e F \d\e Y') ?></span>
					</div>

					<p><?php echo the_content() ?></p>
									
					<div class="fb-like" data-href="<?php echo get_permalink()  ?>" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
					<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

					<!-- Posicione esta tag no cabeçalho ou imediatamente antes da tag de fechamento do corpo. -->
					<script src="https://apis.google.com/js/platform.js" async defer>
						{lang: 'pt-BR'}
					</script>

					<!-- Posicione esta tag onde você deseja que o widget apareça. -->
					<div class="g-follow" data-annotation="bubble" data-height="20" data-href="//plus.google.com/u/0/102217947050480826231" data-rel="author"></div>
					
					<div class="paginador">
						<?php previous_post('%','<i class="fa fa-chevron-left" aria-hidden="true"></i> anterior', 'no') ?>
						<?php next_post('%','	próximo 	<i class="fa fa-chevron-right" aria-hidden="true"></i>	', 'no') ?>				
					</div>

				</div>
	
						<?php
						//If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						
						endif;
					?>
			</div>
		</div>
		
	</section>
</div>

<?php
get_footer();
 include (TEMPLATEPATH . '/inc/scriptMapa.php');