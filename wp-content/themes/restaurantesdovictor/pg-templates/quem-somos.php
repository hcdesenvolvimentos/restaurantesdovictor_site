<?php
session_start();
/**
 * Template Name: Quem Somos
 * Description: Página inicial do site  Restaurantes do Victor
 *
 * @package Restaurantes_do_Victor
 */



$quemsomsoFoto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );

$quemsomsoFoto = $quemsomsoFoto[0];

$quemSomos_banner   =	rwmb_meta('Restaurantesdovictor_quemSomos_banner'); 

$quemSomos_video_banner   =	rwmb_meta('Restaurantesdovictor_quemSomos_video_banner'); 

$checkbox_quemsomos   =	rwmb_meta('Restaurantesdovictor_checkbox_quemsomos'); 

$quemSomos_historia   =	rwmb_meta('Restaurantesdovictor_quemSomos_historia'); 

$quemSomos_comoSurgiu = rwmb_meta('Restaurantesdovictor_quemSomos_comoSurgiu'); 


// PEGANDO TÍTULO E TEXTO SEPARADO POR BARRA
$historia = explode("|", $quemSomos_historia);
$comoSurgiu = explode("|", $quemSomos_comoSurgiu);

// VERIFICANDO FOTO BANNER
foreach ($quemSomos_banner as $quemSomos_banner) {
	$quemSomos_banner = $quemSomos_banner;

}
get_header(); ?>
<!-- VERIFICAÇÃO PARA RECEBER TAXONOMYA DE CADA FRANQUIA -->
<?php 
	if ($_SESSION['nomeFranquia'] == "Bar do Victor") {	

		$taxonomia = 'bar-do-victor-sao-lourenco';
		$taxonomiaIntegrantes = 'bar-do-victor-sao-lourenco';
		$estabelecimento = $configuracao['opt-nome-estabelecimento'];
		$endereco = $configuracao['opt-endereco'];
		

	} else if ($_SESSION['nomeFranquia'] == "Bistrô do Victor") {

		$taxonomia = 'bistro-do-victor';
		$taxonomiaIntegrantes = 'bistro-do-victor';
		$endereco = $configuracao['opt-endereco-bistro'];
		$estabelecimento = $configuracao['opt-nome-estabelecimento-bistro'];

	} else if ($_SESSION['nomeFranquia'] == "Petiscaria do Victor") {
		$endereco = $configuracao['opt-endereco-Petiscaria'];
		$taxonomia = 'petiscaria-do-victor';
		$taxonomiaIntegrantes = 'petiscaria-do-victor';
		$estabelecimento = $configuracao['opt-nome-estabelecimento-Petiscaria'];
		

	} else if ($_SESSION['nomeFranquia'] == "Bar do Victor &#8211; Praça da Espanha") {

		$taxonomia = 'bar-do-victor-praca-da-espanha';
		$taxonomiaIntegrantes = 'bar-do-victor-praca-da-espanha';
		$endereco = $configuracao['opt-endereco-espanha'];
		$estabelecimento = $configuracao['opt-nome-estabelecimento-espanha'];
		

	}
?>


	<div class="pg pg-quem-somos" style="display:">
		
		<?php 
			// VERIFICANDO CHECKBOX VÍDEO OU FOTO
			if ($checkbox_quemsomos  == 1):			
			
		?>

		<div class="videoGeral">
			<video  preload="auto" autoplay="" loop="" muted="" src="<?php echo $quemSomos_video_banner ?>"></video>
		</div>

		<?php else: ?>

		<div class="bg-quem-somos"  style="display:;background:url(<?php echo $quemSomos_banner['full_url'] ?>)"></div>
		
		<?php endif; ?>

		<!-- ÁREA QUEM SOMOS DESCRIÇÃO -->
		<section class="container area-quemsomos">
			<div class="row">
				<div class="col-md-6">
					<div class="texto-quem-somos">
						<h2><?php echo $estabelecimento ?> </h2>
						<span><?php echo $endereco  ?> <i class="fa fa-map-marker" aria-hidden="true"></i></span>
						<p><?php echo get_the_content() ?> </p>
					</div>
				</div>

				<div class="col-md-6 text-center">
					<div class="area-historia-como-surgiu">					
						<ul>
							<li>
								<span><?php echo $historia[0];  ?></span>
								<p><?php echo $historia[1]; ?></p>
							</li>
							<li class="direito">
								<span><?php echo $comoSurgiu[0];  ?></span>
								<p><?php echo $comoSurgiu[1]; ?></p>
							</li>
						</ul>
					</div>					
				</div>
			</div>
		</section>

		<!-- LINHA DO TEMPO -->
		<section class=" area-linhado-tempo">
				<h6 class="hidden">Linha do Tempo</h6>
			
				<div class="linhaTempo">
				</div>
				
				<ul>
					<?php 
						$x = 0;
						// LOOP DE 	LINHA DO TEMPO					
						$linhadotempo = new WP_Query(array(
							'post_type'     => 'linhadotempo',
							'posts_per_page'   => -1,
							'tax_query'     => array(
								array(
									'taxonomy' => 'categorialinhadotempo',
									'field'    => 'slug',
									'terms'    => $taxonomia ,
									)
								)
							)
						);

						while ( $linhadotempo->have_posts() ) : $linhadotempo->the_post();
						$ano = rwmb_meta('Restaurantesdovictor_LinhadotempoAno'); 
						$descricao = rwmb_meta('Restaurantesdovictor_LinhadotempoDescricao'); 

						// IF PARA FAZER MUDANÇA NO LAYOUT
						if($x % 2 == 0):
					 ?>

					<li class="linhaBaixo" title="<?php echo $ano ?>">
						<div class="textoLinhaPar" title="<?php echo $ano ?>">
							<span><?php echo get_the_title() ?></span>
							<p><?php echo $descricao ?></p>
							
						</div>
						
					</li>

					<?php else: ?>
					
					<li class="linhaCima" title="<?php echo $ano ?>">
						<div class="textoLinhaPar" title="<?php echo $ano ?>">
							<span><?php echo get_the_title() ?></span>
							<p><?php echo $descricao ?></p>
							
						</div>
						
					</li>

					<?php endif; $x++;endwhile;wp_reset_query(); ?>
				</ul>
			

		</section>
	</div>	

<?php get_footer(); 
 include (TEMPLATEPATH . '/inc/scriptMapa.php');
?>