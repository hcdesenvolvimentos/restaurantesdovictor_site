<?php 
session_start();
if ( have_posts() ) : while( have_posts() ) : the_post();
	$nomeFranquia = get_the_title();
	endwhile;
endif;
//$_SESSION["nomeFranquia"]= $nomeFranquia;
?>
<?php
/**
 * Template Name: Franquia
 * Description: Página inicial do site  Restaurantes do Victor
 *
 * @package Restaurantes_do_Victor
 */
// LOOP DO CONTENT 
if ( have_posts() ) : while( have_posts() ) : the_post();
$descricao = get_the_content();
 endwhile; endif;  


// FOTO METABOX PÁGINA DE FRANQUIAS
 $inicial_eventos   =	rwmb_meta('Restaurantesdovictor_inicial_eventos'); 
 $inicial_reserva   =	rwmb_meta('Restaurantesdovictor_inicial_reserva');
 $formularioReserva   =	rwmb_meta('Restaurantesdovictor_inicial_reserva_formulario');

// FOREACH FOTOS
foreach ($inicial_eventos as $inicial_eventos) {
  	$inicial_eventos = $inicial_eventos;
 } 
foreach ($inicial_reserva as $inicial_reserva) {
	$inicial_reserva = $inicial_reserva;
} 

get_header(); ?>


<!-- VERIFICAÇÃO PARA RECEBER TAXONOMYA DE CADA FRANQUIA -->
<?php 
	if ($_SESSION['nomeFranquia'] == "Bar do Victor") {	
		$estabelecimento = $configuracao['opt-nome-estabelecimento'];
		$endereco = $configuracao['opt-endereco'];
		$taxonomia = 'bar-do-victor-sao-lourenco';
		$taxonomiaCardapio = 'categoriaCardapio';
		$urlClube = 'clube-bar-do-victor';
		$endereco = $configuracao['opt-endereco'];
		$iconMapa = $configuracao['opt-icon-mapa']['url'];
		$corCasa = "#ffcb04";
		

	} else if ($_SESSION['nomeFranquia'] == "Bistrô do Victor") {
		$estabelecimento = $configuracao['opt-nome-estabelecimento-bistro'];
		$endereco = $configuracao['opt-endereco-bistro'];
		$taxonomia = 'bistro-do-victor';
		$taxonomiaCardapio = 'categoriaBistro';
		$urlClube = 'clube-bistro-do-victor';

		$endereco = $configuracao['opt-endereco-bistro'];
		$iconMapa = $configuracao['opt-icon-mapa-bistro']['url'];
		$corCasa = "#fa8621";
		

	} else if ($_SESSION['nomeFranquia'] == "Petiscaria do Victor") {
		$estabelecimento = $configuracao['opt-nome-estabelecimento-Petiscaria'];
		$endereco = $configuracao['opt-endereco-Petiscaria'];
		$taxonomia = 'petiscaria-do-victor';
		$taxonomiaCardapio = 'categoriaPetiscaria';
		$urlClube = 'clube-petiscaria-do-victor';

		$endereco = $configuracao['opt-endereco-Petiscaria'];
		$iconMapa = $configuracao['opt-icon-mapa-Petiscaria']['url'];
		$corCasa = "#8d2a2f";
		$color = "#fff";
		

	} else if ($_SESSION['nomeFranquia'] == "Bar do Victor &#8211; Praça da Espanha") {
		$estabelecimento = $configuracao['opt-nome-estabelecimento-espanha'];
		$endereco = $configuracao['opt-endereco-espanha'];
		$taxonomia = 'bar-do-victor-praca-da-espanha';
		$taxonomiaCardapio = 'categoriaBarespanha';
		$urlClube = 'clube-bar-do-victor-praca-espanha';

		$endereco = $configuracao['opt-endereco-espanha'];
		$iconMapa = $configuracao['opt-icon-mapa-espanha']['url'];
		$corCasa = "#56adb5";
		

	}
?>
	
	<title><?php echo get_the_title(); ?> </title>
	<div class="pg pg-inicial">
				
		<!-- ITEM DESTAQUE   -->
		<div class="carrossel-destaque-inicial" >
			<div class="areaAnimacaoTopo">
				<button id="btnCarrossenDestaqueF" class="btnCarrossenDestaque btnCarrossenDestaqueF"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
				<button id="btnCarrossenDestaqueT" class="btnCarrossenDestaque btnCarrossenDestaqueT"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
				
				<!-- CARROSSEL VIDEOS -->
				<div id="carrossel-destaque-inicial" class="owl-Carousel">
					<?php 
					// LOOP DE DESTAQUES					
					$postDestque = new WP_Query(array(
						'post_type'     => 'destaque',
						'posts_per_page'   => -1,
						'tax_query'     => array(
							array(
								'taxonomy' => 'categoriaDestaque',
								'field'    => 'slug',
								'terms'    => $taxonomia,
								)
							)
						)
					);

					// LOOP DE DESTAQUE DA CATEGORIA MARCADA
					$i = 0;
					while ( $postDestque->have_posts() ) : $postDestque->the_post();
						$fotopostDestque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                    	$fotopostDestque = $fotopostDestque[0];	
                    	$verificacao = rwmb_meta('Restaurantesdovictor_checkbox_destaque'); 
                    	$urlVideo = rwmb_meta('Restaurantesdovictor_video_destaque'); 
                    	$link = rwmb_meta('Restaurantesdovictor_link_post'); 

                    	
                  
					?>
					<a  href="<?php echo $link  ?>" target="_blank" class="item" style="background:url(<?php echo $fotopostDestque ?>)">
						<?php if ($verificacao == "1"): ?>
						<video loop= autoplay muted class="background" style="background:url(<?php echo $fotoDestaque ?>)" id="video">
							<source src="<?php echo $urlVideo ?>" type="video/mp4">
						</video>
						<?php endif; ?>
					</a>
				<?php endwhile; ?>
				</div>
			</div>
		</div>		

		<!-- ÁREA RESERVAS E EVENTOS -->	
		<section class="area-eventos-reservas-inicial">
			<h6 class="hidden">Reservas para eventos</h6>
			<div class="row">
				
				<div class="col-md-12">

					<div class="eventos-fotos-inicial" style="background:url(<?php echo  $inicial_reserva['full_url'] ?>)">
						<div class="lente-area-eventos-reservas-inicial">
							<!-- link do a  "#myModal" data-toggle="modal" data-target="#myModal" -->
							<?php if ($formularioReserva != ""): ?>
								<a href="#myModal" data-toggle="modal" data-target="#myModal" class="reservaicon">Faça sua reserva!</a>
							<?php else: ?>
								<a href="http://restaurantesvictor.com.br/contato-petiscaria-do-victor/" class="reservaicon">Faça sua reserva!</a>
								
							<?php endif ?>
						</div>
					</div>
				
				</div>
				
			</div>
		</section>

		<!-- SOBRE O BAR  -->
		<section class="container area-sobre-inicial">
			<span><?php echo  $estabelecimento ?></span>
			<!-- <small><?php //echo  $endereco ?><i class="fa  fa-map-marker" aria-hidden="true"></i></small> -->
			<p><?php echo $descricao ?></p>
		</section>
				

		<!-- ÁREA CARDÁPIO -->
		<section class=" area-cardapio-inicial" style="display: none">

			<!-- TÍTULO PÁGINA  -->
			<div class="titulo-paginas">
				<p>Cardápio</p>
			</div>

			<!-- CARROSSEL DE  CARDÁPIO -->
			<div class="carrossel-cardapio-inicial">	
				<div id="carrossel-cardapio-inicial" class="owl-Carousel">
					<?php					

						// LISTA AS CATEGORIAS PAI DOS SABORES
						$categoriasCardapio = get_terms( $taxonomiaCardapio, array(
							'orderby'    => 'count',
							'hide_empty' => 0,
							'parent'	 => 0
						));
						
						
						
						foreach ($categoriasCardapio as $categoriaCardapio):
							//NOME DA CATEGORIAS// 
							$nome = $categoriaCardapio->name;	
							// DESCRIÇÃO RECUPERANDO URL DO ICONE
							$descricao = $categoriaCardapio->description;
							// IMAGEM DA CATEGORIA 
							$categoriaAtivaImg = z_taxonomy_image_url($categoriaCardapio->term_id);
							
					?>
					<!-- ITEM CARDÁPIO   -->
					<div class="item">
						<!-- LINK -->
						<a href="<?php echo get_category_link($categoriaCardapio->term_id); ?>">
							<!-- ICONE -->
							<div class="cardapio-nome-inicial"><img src="<?php  echo $descricao ?>" class="img-responsive" alt="<?php echo $nome ?>">
								<!-- NOME -->
								<h3><?php echo $nome ?></h3>
							</div>
							<!-- IMAGEM DA CATEGORIA -->
							<div class="cardapio-foto-inicial" style="background:url(<?php echo $categoriaAtivaImg ?>)">
								<div class="lente-cardapio-inicial">
									<span>Ver pratos</span>
								</div>
							</div>						
						</a>
					</div>

					<?php  endforeach ;?>				
					
				</div>
				<!-- BOTÕES -->
				<div class="text-center">
					<button class="navegacaoCardapioInicialFrent hvr-bounce-to-left"><i class="fa fa-angle-left"></i></button>
					<button class="navegacaoCardapioInicialTras hvr-bounce-to-right"><i class="fa fa-angle-right"></i></button>
				</div>
			</div>
		</section>

		<!-- ÁREA CLUBE  -->
		<section class=" area-clube-inicial">
			<h6 class="hidden">Club Victor</h6>
			<!-- TÍTULO PÁGINA  -->
			<div class="titulo-paginas">
				<p>Clube Victor</p>
			</div>

			<ul>
			<?php 
				// LOOP DE POST CLUBE
				$clube = new WP_Query( array( 'post_type' => 'clube', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => 3 ) );
				while ( $clube->have_posts() ) : $clube->the_post();
					$fotoClube = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$fotoClube = $fotoClube[0];

			 ?>
				<li>  
					<a href="<?php echo home_url($urlClube); ?>">
						<!-- FOTO DESTACADA -->
						<div class="clube-foto-inical" style="background:url(<?php echo $fotoClube ?>)">
							<div class="lente">
								<!-- TÍTULO DO POST -->
								<p><?php echo get_the_title() ?></p>
								<!-- BREVE DESCRIÇÃO -->
								// <!-- <p><?php//customExcerpt(50); ?></p> -->
							</div>
						</div>	
					</a>				
				</li>
			<?php endwhile;wp_reset_query(); ?>
			</ul>

			<a href="<?php echo home_url($urlClube); ?>" class="quero-participar hvr-sweep-to-right">Saiba mais</a>
		</section>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      
	       <div class="modal-body">
	      	    <button type="button" class="close" data-dismiss="modal">&times;</button>
	  		<div class="pg pg-contato">
				
				<p class="subtitulo-contato text-center">SOLICITAR RESERVA</p>							

				<div class="formulario-contato">

					<?php //echo do_shortcode($formularioReserva);
						echo $formularioReserva;
				 	?>

				</div>
			
			</div>

	      </div>
	      
	    </div>
	  </div>
	</div>
	
	

<?php get_footer(); ?>
<?php 

 // FUNÇÃO MAPA 

$curl = curl_init();

curl_setopt_array($curl, array(
   CURLOPT_RETURNTRANSFER => 1,
   CURLOPT_URL => 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBY7oA-HMU8HYmWtlQ1nJxWsWLTtpctYt0&address='.urlencode($endereco).'&sensor=false',
));
$geo 	= curl_exec($curl);

$geo   = json_decode($geo, true);

$enderecoGoogle = 'https://www.google.com.br/maps/place/' . urldecode($endereco);


if ($geo['status'] == 'OK') {
	$latitude = $geo['results'][0]['geometry']['location']['lat'];
	$longitude = $geo['results'][0]['geometry']['location']['lng'];
}


 ?>
<!-- FUNÇÃO PARA ADICIONAR ICONE NO MAPA -->
<script>

var map;
//POSICIONAMENTO DOS ESTABALECIMENTOS
var pierDoVictor = {lat: <?php echo $latitude; ?>, lng: <?php echo $longitude; ?>};


//ICONES DOS ESTABELECIMENTOS
var iconePierDoVictor = "<?php echo $iconMapa; ?>";



function initMap() {

	//MAPA
	map = new google.maps.Map(document.getElementById('map'), {
		center: pierDoVictor,
		zoom: 16,
		disableDefaultUI: true,
		zoomControl: true,
		scrollwheel: false,
		draggable: false, 
		disableDoubleClickZoom: true,
		styles: [			
			{
				"featureType":"landscape",
				"elementType":"geometry.fill",
				"stylers":[{"color":"#ebebeb"}]
			},
			{
			    "featureType": "administrative",
			    "elementType": "labels",
			    "stylers": [
			      { "visibility": "off" }
			    ]
		  	},{
			    "featureType": "poi",
			    "elementType": "labels",
			    "stylers": [
			      { "visibility": "off" }
			    ]
		  	},{
			    "featureType": "water",
			    "elementType": "labels",
			    "stylers": [
			      { "visibility": "off" }
			    ]
		  	}
  		]
	});

	//MARKERS OU PINS DO MAPA
	var markerPierDoVictor = new google.maps.Marker({
	    position: pierDoVictor,
	    icon: iconePierDoVictor,
	    map: map
	});


}

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBY7oA-HMU8HYmWtlQ1nJxWsWLTtpctYt0&callback=initMap" async defer></script>