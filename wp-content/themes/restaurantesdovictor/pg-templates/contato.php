<?php
session_start();
/**
 * Template Name: Contato
 * Description: Página inicial do site  Restaurantes do Victor
 *
 * @package Restaurantes_do_Victor
 */
$formulario  = rwmb_meta('Restaurantesdovictor_contat_formulario'); 

$fotoContato = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$fotoContato = $fotoContato[0];
global $configuracao;

if ($_SESSION['nomeFranquia'] == "Bar do Victor") {	

	$logoFraquias = $configuracao['opt-logo']['url'];
	$titulo = $configuracao['opt-frase-titulo'];
	$endereco = $configuracao['opt-endereco'];
	$frase = $configuracao['opt-frase'];
	$iconMapa = $configuracao['opt-icon-mapa']['url'];
	$formularioContato = $configuracao['opt-formulario-mapa'];
	$horario = $configuracao['opt-horarioF'];

} else if ($_SESSION['nomeFranquia'] == "Bistrô do Victor") {

	$logoFraquias = $configuracao['opt-logo-bistro']['url'];
	$titulo = $configuracao['opt-frase-titulo-bistro'];
	$endereco = $configuracao['opt-endereco-bistro'];
	$frase = $configuracao['opt-frase-bistro'];
	$iconMapa = $configuracao['opt-icon-mapa-bistro']['url'];
	$formularioContato = $configuracao['opt-formulario-bistro'];
	$horario = $configuracao['opt-horarioB'];

} else if ($_SESSION['nomeFranquia'] == "Petiscaria do Victor") {

	$logoFraquias = $configuracao['opt-logo-Petiscaria']['url'];
	$titulo = $configuracao['opt-frase-titulo-Petiscaria'];
	$endereco = $configuracao['opt-endereco-Petiscaria'];
	$frase = $configuracao['opt-frase-Petiscaria'];
	$iconMapa = $configuracao['opt-icon-mapa-Petiscaria']['url'];
	$formularioContato = $configuracao['opt-formulario-Petiscaria'];
	$horario = $configuracao['opt-horarioP'];
	$fomrularioCompras = $configuracao['opt-fomrularioCompras'];
	$fomrularioFaleconosco = $configuracao['opt-fomrularioFaleconosco'];
	

} else if ($_SESSION['nomeFranquia'] == "Bar do Victor &#8211; Praça da Espanha") {

	$logoFraquias = $configuracao['opt-logo-espanha']['url'];
	$titulo = $configuracao['opt-frase-titulo-espanha'];
	$endereco = $configuracao['opt-endereco-espanha'];
	$frase = $configuracao['opt-frase-espanha'];
	$iconMapa = $configuracao['opt-icon-mapa-espanha']['url'];
	$formularioContato = $configuracao['opt-formulario-espanha'];
	$horario = $configuracao['opt-horarioS'];

}
get_header(); ?>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modalContato" role="document">
    <div class="modal-content">
      
       <div class="modal-body">
      	<button type="button" class="close" data-dismiss="modal">&times;</button>

  		<div class="pg pg-contato modalCOntato">			
			<?php if($_SESSION['nomeFranquia'] == "Petiscaria do Victor"): ?>
			<div class="formulario-contato trabalheConosco">
				<p class="subtitulo-contato text-center" id="nomeFormulario" >Trabalhe Conosco</p>		
				<?php echo do_shortcode($fomrularioFaleconosco); ?>
			</div>
			<?php else: ?>
			<div class="formulario-contato trabalheConosco">
				<p class="subtitulo-contato text-center" id="nomeFormulario" >Trabalhe Conosco</p>		
				<?php echo do_shortcode('[contact-form-7 id="442" title="Formulário Trabalhe Conosco"]'); ?>
			</div>
			<?php endif; ?>


			<?php if($_SESSION['nomeFranquia'] == "Petiscaria do Victor"): ?>
			<div class="formulario-contato compras">
				<p class="subtitulo-contato text-center" id="nomeFormulario" >Compras</p>		
				<?php echo do_shortcode($fomrularioCompras);?>
			</div>
			<?php else: ?>
			<div class="formulario-contato compras">
				<p class="subtitulo-contato text-center" id="nomeFormulario" >Compras</p>		
				<?php echo do_shortcode('[contact-form-7 id="443" title="Fomulário de compras"]'); ?>
			</div>
			<?php endif; ?>

			<div class="formulario-contato adm">
				<p class="subtitulo-contato text-center" id="nomeFormulario" >Administrativo</p>		
				<?php echo do_shortcode('[contact-form-7 id="444" title="Formulário Administrativo"]'); ?>
			</div>

			<div class="formulario-contato sugestao">
				<p class="subtitulo-contato text-center" id="nomeFormulario" >Deixe sua sugestão</p>			
				<?php echo do_shortcode('[contact-form-7 id="442" title="Formulário Trabalhe Conosco"]'); ?>
			</div>
		
		</div>

      </div>
      
    </div>
  </div>
</div>
<div class="pg pg-contato">
	<div class="banner-contato" style="background:url(<?php  echo $fotoContato ?>)">
		<div class="nome-contato">
			<p>Contato</p>
			
		</div>
	</div>	
	<div class="row areaFomrulario">
		<div class="col-md-7">
			<p class="subtitulo-contato"><?php echo get_the_content() ?> <img src="<?php bloginfo('template_directory'); ?>/svg/aviao.svg" class="img-responsive imgContato" alt=""></p>
			<div class="formulariosIndividuais text-center">
				<ul>
					<li  data-toggle="modal" data-target="#myModal" class="tipoForm" data-tituloItem="Trabalhe Conosco" data-title="trabalheConosco">
						<div class="icon">
							<img src="<?php bloginfo('template_directory'); ?>/svg/trabalheconosco.svg" alt="" class="img-responsive">
						</div>

						<span>Trabalhe Conosco</span>
					</li>
					<li  data-toggle="modal" data-target="#myModal" class="tipoForm" data-tituloItem="Compras" data-title="compras">
						<div class="icon">
							<img src="<?php bloginfo('template_directory'); ?>/svg/compras.svg" alt="" class="img-responsive">
						</div>

						<span>Compras</span>
					</li>
					<li  data-toggle="modal" data-target="#myModal" class="tipoForm" data-tituloItem="Administrativo" data-title="adm">
						<div class="icon">
							<img src="<?php bloginfo('template_directory'); ?>/svg/adm.svg" alt="" class="img-responsive">
						</div>

						<span>Administrativo</span>
					</li>
					<li  data-toggle="modal" data-target="#myModal" class="tipoForm" data-tituloItem="Deixe sua sugestão" data-title="sugestao">
						<div class="icon">
							<img src="<?php bloginfo('template_directory'); ?>/svg/sugestao.svg" alt="" class="img-responsive">
						</div>

						<span>Deixe sua sugestão</span>
					</li>
				</ul>
			</div>
			<div class="formulario-contato">

				<?php echo do_shortcode($formulario); ?>

			</div>
		</div>
		<div class="col-md-5 text-center">
			
			<div class="info-contato-rodape">
				
				<img src="<?php echo  $logoFraquias  ?>" class="img-responsive" alt="">
				<!-- <p>Entre em contato </p> -->
				<span><?php echo $titulo ?></span>
				<?php 
					$horarioFormatados = explode(".", $horario);
					foreach ($horarioFormatados as $horarioFormatado) {
						$horarioFormatado = $horarioFormatado;
					
				 ?>
				<div class="horarios"><?php echo  $horarioFormatado  ?></div>
				<?php } ?>
				<small><?php echo  $endereco  ?></small>
				<small><strong><?php echo  $frase  ?></strong></small>
				
			</div>

			
		</div>
	</div>
</div>		

<?php get_footer(); 
 include (TEMPLATEPATH . '/inc/scriptMapa.php');
?>
