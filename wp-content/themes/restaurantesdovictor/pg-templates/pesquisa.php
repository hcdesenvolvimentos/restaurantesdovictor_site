<?php
session_start();
/**
 * Template Name: Pesquisa
 * Description: Página inicial do site  Restaurantes do Victor
 *
 * @package Restaurantes_do_Victor
 */

get_header(); ?>


<style type="text/css">
	.pg-pesquisa{
	   
	        padding-top: 207px;

	}
		.pg-pesquisa iframe{
			width: 100%;
			min-height: 985px;
		}
</style>

<div class="pg-pesquisa">
	<iframe src="https://docs.google.com/a/pierdovictor.com.br/forms/d/e/1FAIpQLSf6qIAxNmqRi5kV2CFrCvITfL_m9DU7Ue_Ghaeb0fdRV0NDUQ/viewform?embedded=true" width="760" height="500" frameborder="0" marginheight="0" marginwidth="0">Carregando…</iframe>
</div>
<?php get_footer(); ?>
<?php 
	$logoFraquias = $configuracao['opt-logo-restaurantes']['url'];

	$titulo = $configuracao['opt-frase-titulo-restaurantes'];

	

	$iconMapa = $configuracao['opt-icon-mapa-restaurantes']['url'];

	$formularioContato = $configuracao['opt-formulario-restaurantes'];



	// ENDEREÇO DOS RESTAURANTES 

	$endereco = $configuracao['opt-endereco-restaurantes'];

	$frase = $configuracao['opt-frase-restaurantes'];



	// BAR DO VICTOR PRAÇA DA ESPANHA 

	$enderecoE = $configuracao['opt-endereco-espanha'];

	$iconMapaE = $configuracao['opt-icon-mapa-espanha']['url'];



	//PETISCARIA

	$enderecoP = $configuracao['opt-endereco-Petiscaria'];	

	$iconMapaP = $configuracao['opt-icon-mapa-Petiscaria']['url'];



	// BISTRÔ

	$enderecoB = $configuracao['opt-endereco-bistro'];	

	$iconMapaB = $configuracao['opt-icon-mapa-bistro']['url'];



	//BAR DO VICTOR SÃO LOURENÇO 

	$enderecoS = $configuracao['opt-endereco'];

	$iconMapaS = $configuracao['opt-icon-mapa']['url'];



 // FUNÇÃO MAPA 



$curl = curl_init();



curl_setopt_array($curl, array(

   CURLOPT_RETURNTRANSFER => 1,

   CURLOPT_URL => 'http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($endereco).'&sensor=false',

));

$geo = curl_exec($curl);



curl_close($curl);



$geo   = json_decode($geo, true);



///





// BAR DO VICTOR SÃO LOURENÇO 



$curl = curl_init();



curl_setopt_array($curl, array(

   CURLOPT_RETURNTRANSFER => 1,

   CURLOPT_URL => 'http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($enderecoS).'&sensor=false',

));

$geoS 	= curl_exec($curl);



curl_close($curl);



$geoS   = json_decode($geoS, true);



///





// BISTRÔ

$curl = curl_init();



curl_setopt_array($curl, array(

   CURLOPT_RETURNTRANSFER => 1,

   CURLOPT_URL => 'http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($enderecoB).'&sensor=false',

));

$geoB 	= curl_exec($curl);



curl_close($curl);



$geoB   = json_decode($geoB, true);



///





//PETISCARIA



$curl = curl_init();



curl_setopt_array($curl, array(

   CURLOPT_RETURNTRANSFER => 1,

   CURLOPT_URL => 'http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($enderecoP).'&sensor=false',

));

$geoP 	= curl_exec($curl);



$geoP   = json_decode($geoP, true);



///



// BAR DO VICTOR PRAÇA DA ESPANHA 



$curl = curl_init();



curl_setopt_array($curl, array(

   CURLOPT_RETURNTRANSFER => 1,

   CURLOPT_URL => 'http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($enderecoE).'&sensor=false',

));

$geoE 	= curl_exec($curl);



$geoE   = json_decode($geoE, true);





///



if (isset($geo['results'])) {



	$latitude = $geo['results'][0]['geometry']['location']['lat'];

	$longitude = $geo['results'][0]['geometry']['location']['lng'];

}



//BAR DO VICTOR SÃO LOURENÇO 

if (isset($geoS['results'])) {

	$latitudeS = $geoS['results'][0]['geometry']['location']['lat'];

	$longitudeS = $geoS['results'][0]['geometry']['location']['lng'];

}





//BAR DO VICTOR SÃO LOURENÇO 

if (isset($geoB['results'])) {

	$latitudeB = $geoB['results'][0]['geometry']['location']['lat'];

	$longitudeB = $geoB['results'][0]['geometry']['location']['lng'];

}



//PETISCARIA

if (isset($geoP['results'])) {

	$latitudeP = $geoP['results'][0]['geometry']['location']['lat'];

	$longitudeP = $geoP['results'][0]['geometry']['location']['lng'];

}



// BAR DO VICTOR PRAÇA DA ESPANHA 

if (isset($geoE['results'])) {

	$latitudeE = $geoE['results'][0]['geometry']['location']['lat'];

	$longitudeE = $geoE['results'][0]['geometry']['location']['lng'];

}



?>

<!-- FUNÇÃO PARA ADICIONAR ICONE NO MAPA -->
<script>

	var map;
	//POSICIONAMENTO DOS ESTABALECIMENTOS
	var pierDoVictor = {lat: <?php echo $latitude; ?>, lng: <?php echo $longitude; ?>};

	//BAR DO VICTOR SÃO LOURENÇO 
	var barVictorSaolourenco = {lat: <?php echo $latitudeS; ?>, lng: <?php echo $longitudeS; ?>};

	//BAR DO VICTOR SÃO LOURENÇO 
	var iconbarVictorSaolourenco = "<?php echo $iconMapaS; ?>";

	// BISTRÔ
	var bistro = {lat: <?php echo $latitudeB; ?>, lng: <?php echo $longitudeB; ?>};

	// BISTRÔ
	var iconBistro = "<?php echo $iconMapaB; ?>";

	//PETISCARIA
	var petiscaria = {lat: <?php echo $latitudeP; ?>, lng: <?php echo $longitudeP; ?>};

	// BAR DO VICTOR PRAÇA DA ESPANHA 
	var restauranteEspanha = {lat: <?php echo $latitudeE; ?>, lng: <?php echo $longitudeE; ?>};

	// BISTRÔ
	var iconiconMapaE = "<?php echo $iconMapaE; ?>";

	//ICONES DOS ESTABELECIMENTOS
	var iconePierDoVictor = "<?php echo $iconMapa; ?>";

	//LINK PIN
	var endereco = "<?php echo 'https://www.google.com.br/maps/place/' . urldecode($geo['results'][0]['formatted_address']); ?>";

	var enderecos = "<?php echo 'https://www.google.com.br/maps/place/' . urldecode($geoS['results'][0]['formatted_address']); ?>";

	var enderecob = "<?php echo 'https://www.google.com.br/maps/place/' . urldecode($geoB['results'][0]['formatted_address']); ?>";

	var enderecop = "<?php echo 'https://www.google.com.br/maps/place/' . urldecode($geoP['results'][0]['formatted_address']); ?>";

	var enderecoe = "<?php echo 'https://www.google.com.br/maps/place/' . urldecode($geoE['results'][0]['formatted_address']); ?>";

	// PETISCARIA
	var iconptiscaria = "<?php echo $iconMapaP; ?>";

	// BAR DO VICTOR PRAÇA DA ESPANHA 
	var iconrestauranteEspanha = "<?php echo $iconMapaP; ?>";

	function initMap() {

		//MAPA
		map = new google.maps.Map(document.getElementById('map'), {
			center: petiscaria,
			zoom: 12,
			disableDefaultUI: true,
			zoomControl: true,
			scrollwheel: false,
			draggable: false, 
			disableDoubleClickZoom: true,
			styles: [			
				{

					"featureType":"landscape",

					"elementType":"geometry.fill",

					"stylers":[{"color":"#ebebeb"}]

				},

				{

				    "featureType": "administrative",

				    "elementType": "labels",

				    "stylers": [

				      { "visibility": "off" }

				    ]

			  	},{

				    "featureType": "poi",

				    "elementType": "labels",

				    "stylers": [

				      { "visibility": "off" }

				    ]

			  	},{

				    "featureType": "water",

				    "elementType": "labels",

				    "stylers": [

				      { "visibility": "off" }

				    ]

			  	}

	  		]

		});



		//MARKERS OU PINS DO MAPA
		var markerPierDoVictor = new google.maps.Marker({
		    position: pierDoVictor,
		    icon: iconePierDoVictor,
		    map: map
		});

		google.maps.event.addListener(markerPierDoVictor, 'click', function(e) {
				 window.open(endereco);
				// alert(endereco);
		});

		//BAR DO VICTOR SÃO LOURENÇO 
		var markerbarSaoLourenco = new google.maps.Marker({
		    position: barVictorSaolourenco,
		    icon: iconbarVictorSaolourenco,
		    map: map

		});

		google.maps.event.addListener(markerbarSaoLourenco, 'click', function(e) {
			 window.open(enderecos);
		});

		// BISTRÔ
		var marKerBistro = new google.maps.Marker({
		    position: bistro,
		    icon: iconBistro,
		    map: map
		});

		google.maps.event.addListener(marKerBistro, 'click', function(e) {
			 window.open(enderecob);
		});

		// BISTRÔ
		var marKerpetiscaria = new google.maps.Marker({
		    position: petiscaria,
		    icon: iconptiscaria,
		    map: map
		});

		google.maps.event.addListener(marKerpetiscaria, 'click', function(e) {
			 window.open(enderecop);
		});

		// BAR DO VICTOR PRAÇA DA ESPANHA 
		var marKerptacadaespanha = new google.maps.Marker({
		    position: restauranteEspanha,
		    icon: iconiconMapaE,
		    map: map
		});
		google.maps.event.addListener(marKerptacadaespanha, 'click', function(e) {

			 window.open(enderecoe);
		});
	}

</script>

  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBY7oA-HMU8HYmWtlQ1nJxWsWLTtpctYt0&callback=initMap" async defer></script>