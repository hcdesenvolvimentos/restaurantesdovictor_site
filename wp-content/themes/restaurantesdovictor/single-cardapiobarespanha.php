<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Restaurantes_do_Victor
 */
$fotocardapio = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$fotocardapio = $fotocardapio[0];
$Subtitulo = rwmb_meta('Restaurantesdovictor_prato_Subtitulo_espanha');
$detalhe = rwmb_meta('Restaurantesdovictor_prato_detalhe_espanha');
get_header(); ?>

<div class="pg pg-cardapio" style="display: none">
		
	<!-- CARROSSEL DE PRATOS  -->
	<section class="area-carrossel-pratos-cardapio" style="background:url(<?php bloginfo('template_directory'); ?>/img/bg-cardapio.png)">
		
		<!-- TÍTULO PÁGINA  -->
		<div class="titulo-paginas">
			<p>Cardápio</p>
			<span><?php echo $categoriaAtual->name;  ?></span>
		</div>


		<!-- CARROSSEL DE  CARDÁPIO -->
		<div class="carrossel-cardapio">	
			<div id="carrossel-cardapio" class="owl-Carousel">
				<?php
					$i = 0;
					// DEFINE A TAXONOMIA
					$taxonomia = 'categoriaBarespanha';

					// LISTA AS CATEGORIAS PAI DOS SABORES
					$categoriasCardapio = get_terms( $taxonomia, array(
						'orderby'    => 'count',
						'hide_empty' => 0,
						'parent'	 => 0
					));
					
					
					
					foreach ($categoriasCardapio as $categoriaBarespanha):								
							$nome = $categoriaBarespanha->name;	
							$descricao = $categoriaBarespanha->description;
							$slug = $categoriaBarespanha->slug;
							
							$categoriaAtivaImg = z_taxonomy_image_url($categoriaBarespanha->term_id);
						
				?>
				<!-- ITEM CARDÁPIO   -->
				<div class="item" data-hash="<?php echo $i ?>" id="<?php echo $i ?>" data-title="<?php echo $slug ?>">
					<a href="<?php echo get_category_link($categoriaBarespanha->term_id); ?>">
						<div class="cardapio-nome"><img src="<?php echo $descricao  ?>" class="img-responsive" alt=""><h3><?php echo $nome  ?></h3></div>
						<div class="cardapio-foto" style="background:url(<?php echo $categoriaAtivaImg ?>)">
							<div class="lente-cardapio">
								
							</div>
						</div>						
					</a>
				</div>

				<?php   $i++;endforeach ;?>


			</div>
			<!-- BOTÕES -->
			<div class="text-center">
				<button class=" btnCardapioF" id=""><i class="fa fa-angle-left"></i></button>
				<button class=" btnCardapioT" id=""><i class="fa fa-angle-right"></i></button>
			</div>
		</div>

	</section>	
	<!-- ÁREA DOS PRATOS -->
	<section class="area-pratos-cardapio"  id="prato">
		<!-- PRATO -->
		<div class="row prato">
			
			<div class="col-md-6">
				<div class="prato-foto-cardapio" style="background:url(<?php echo $fotocardapio  ?>)">
					<div class="descricao-cardapio">
						<p><?php echo  get_the_title() ?> </p>
						<span><?php echo $Subtitulo  ?></span>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				
				<div class="prato-texto-cardapio">
					<?php echo the_content() ?>
				</div>

				<div class="prato-igredientes-cardapio">
					<ul>
						<?php 
							foreach ($detalhe as $detalhe):
							
							$descricao = explode("|",$detalhe);
						 ?>
						
						<li>
							<img src="<?php echo $descricao[0] ?>" class="img-responsive" alt="">
							<p><?php echo $descricao[1] ?></p>
						</li>
						<?php endforeach; ?>
						
					</ul>
				</div>

			</div>
			
		</div>		
	</section>
</div>
<script>
$(window).load(function(){
$(window).scrollTop(0);
});
	
	window.onload = function () {
		
		$('html, body').animate({
			scrollTop: $('#prato').offset().top
		}, 1000);
	}
</script>
<?php

get_footer();
 include (TEMPLATEPATH . '/inc/scriptMapa.php');
