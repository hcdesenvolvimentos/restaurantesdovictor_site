<!-- PÁGINA MOBILE -->
	<div class="paginaMibile altura" id="topoMobile" style="display:none">
		
		<div class="menuMobile ">
			<ul class="altura">
				
				<!-- TELEFONES -->
				<li>
					<i class="fa fa-phone" aria-hidden="true"></i>
					<span>Ligar</span>
					<div class="menuDrop">
						<?php 
							$i = 0;
							// LOOP DE ESTABELECIMENTO
							$estabelecimento = new WP_Query( array( 'post_type' => 'estabelecimento', 'orderby' => 'rand', 'posts_per_page' => -1 ) );
				            while ( $estabelecimento->have_posts() ) : $estabelecimento->the_post();
							
								// METABOX DE CONTÚDO
								$telefone = rwmb_meta('Restaurantesdovictor_telefone_estabelecimento'); 
								$local = rwmb_meta('Restaurantesdovictor_local_estabelecimento');
						?>	
						<a href="tel:<?php echo $telefone ?>">
							<h3><?php echo  get_the_title() ?></h3>
							<strong><?php echo $local  ?></strong>
						</a>
						<?php $i++; endwhile; wp_reset_query(); ?>
					</div>
				</li>

				<!-- COMO CHEGAR  -->
				<li class="comoChegar">
					<i class="fa fa-map-marker" aria-hidden="true"></i>
					<span>Como Chegar</span>
					<div class="menuDrop">
						<?php 
							$i = 0;
							// LOOP DE ESTABELECIMENTO
							$estabelecimento = new WP_Query( array( 'post_type' => 'estabelecimento', 'orderby' => 'rand', 'posts_per_page' => -1 ) );
				            while ( $estabelecimento->have_posts() ) : $estabelecimento->the_post();
							
								// METABOX DE CONTÚDO
								$endereco = rwmb_meta('Restaurantesdovictor_endereco_estabelecimento'); 
								$local = rwmb_meta('Restaurantesdovictor_local_estabelecimento');
						?>	
						<a href="https://www.google.com.br/maps/place/<?php echo $endereco ?>" target="_blank">
							<h3><?php echo  get_the_title() ?></h3>
							<strong><?php echo $local  ?></strong>
						</a>
						<?php $i++; endwhile; wp_reset_query(); ?>
					</div>
				</li>

				<!-- FOMULÁRIO DE RESERVAS -->
				<li class="reserva">
					
					<i class="fa fa-check-circle" aria-hidden="true"></i>
					<span>Reservas</span>
					<div class="menuDrop">
						<?php 
							$i = 0;
							// LOOP DE ESTABELECIMENTO
							$estabelecimento = new WP_Query( array( 'post_type' => 'estabelecimento', 'orderby' => 'rand', 'posts_per_page' => -1 ) );
				            while ( $estabelecimento->have_posts() ) : $estabelecimento->the_post();
							
								// METABOX DE CONTÚDO
								$formularioReserva = rwmb_meta('Restaurantesdovictor_form_reserva'); 
								$local = rwmb_meta('Restaurantesdovictor_local_estabelecimento');
								$url = rwmb_meta('Restaurantesdovictor_linkpagina_estabelecimento'); 

								if (get_the_title() != "Victor Fish ‘N’ Chips"):
								if ($formularioReserva != ""):
						?>	
						<a href="#myModal" data-toggle="modal" data-target="#<?php echo $i ?>">
							<h3><?php echo get_the_title() ?></h3>
							<strong><?php echo $local ?></strong>
						</a>
						<?php else: ?>
							<a href="<?php echo $url ?>" target="_blank">
								<h3><?php echo get_the_title() ?></h3>
								<strong><?php echo $local ?></strong>
							</a>
						<?php endif;endif; ?>
						<?php $i++; endwhile; wp_reset_query(); ?>
					 
					</div>
					
				</li>
				<!-- HORÁRIOS -->
				<li class="horario">

					<i class="fa fa-clock-o" aria-hidden="true"></i>
					<span>Horários</span>
					<div class="menuDrop horariosDrop">
						<?php 
							$i = 0;
							// LOOP DE ESTABELECIMENTO
							$modais = new WP_Query( array( 'post_type' => 'estabelecimento', 'orderby' => 'rand', 'posts_per_page' => -1 ) );
						    while ( $modais->have_posts() ) : $modais->the_post();

								$local = rwmb_meta('Restaurantesdovictor_local_estabelecimento'); 

								
						?>	
						<a href="#myModal" data-toggle="modal" data-target="#horario<?php echo $i ?>">
							<h3><?php echo get_the_title() ?></h3>
							<strong><?php echo $local ?></strong>
						</a>
						<?php $i++; endwhile; wp_reset_query(); ?>
					</div>
					
				</li>
				<!--  DELIVERY -->
				<li class="delivery" style="display:">
					<i class="fa fa-truck" aria-hidden="true"></i>
					<span>Delivery</span>
					<div class="menuDrop">
					
						<a href="https://www.spoonrocket.com.br/bardovictor" target="_blank">
							<h3>Bar do Victor</h3>
							<strong>São Lourenço</strong>
						</a>
						
						<a href="https://www.spoonrocket.com.br/bardovictorespanha" target="_blank">
							<h3>Bar do Victor</h3>
							<strong>Praça da Espanha</strong>
						</a>
						
						<!-- <a href="http://localhost/projetos/restaurantesdovictor_site/cardapio-petiscaria/" target="_blank">
							<h3>Petiscaria do Victor</h3>
							<strong>Santa Felicidade</strong>
						</a> -->
						
						<a href="http://restaurantesvictor.com.br/bistro-do-victor/" target="_blank">
							<h3>Bistrô do Victor</h3>
							<strong>ParkShoppingBarigüi </strong>
						</a>
						
						<!-- <a href="https://www.spoonrocket.com.br/victorfishnchips" target="_blank">
							<h3>Victor Fish ‘N’ Chips</h3>
							<strong>Shopping Mueller</strong>
						</a> -->

					
					</div>

				</li>

				<!--  EVENTOS -->
				<li class="eventos" style="display:">
					<a href="http://conteudo.restaurantesvictor.com.br/proposta-espaco-para-evento" target="_blank">
						<i class="fa fa-calendar-check-o" aria-hidden="true"></i>
						<span>Eventos</span>
					</a>
				

				</li>
				<!-- LINK RESTAURANTES -->
				<li class="agendar">
					<i class="fa fa-home" aria-hidden="true"></i>
					<span>Visitar Site</span>
					<div class="menuDrop">
					<?php 
						$i = 0;
						// LOOP DE ESTABELECIMENTO
						$modais = new WP_Query( array( 'post_type' => 'estabelecimento', 'orderby' => 'rand', 'posts_per_page' => -1 ) );
					    while ( $modais->have_posts() ) : $modais->the_post();

							$local = rwmb_meta('Restaurantesdovictor_local_estabelecimento'); 
							$url = rwmb_meta('Restaurantesdovictor_linkpagina_estabelecimento'); 
							
					?>	
						<a href="<?php echo $url ?>" target="_blank" >
							<h3><?php echo get_the_title() ?></h3>
							<strong><?php echo $local ?></strong>
						</a>
					<?php $i++; endwhile; wp_reset_query(); ?>
					</div>

				</li>
				
			</ul>
		</div>

		<div class="contentConteudo altura">
			<div class="logo">
				<a href="">
					<img src="<?php bloginfo('template_directory'); ?>/img/logomoBile.png" alt="<?php echo get_the_title() ?>">
				</a>
			</div>

			<!-- CARROSSEL -->
			<section class="carrosselDestaqueFranquias">
				<h6 class="hidden"> Restaurantes Victor</h6>
				<div id="carrossel-eventos-inicial" class="owl-Carousel">
				<?php 
					// LOOP DE DESTAQUE
					$destaqueInicial = new WP_Query( array( 'post_type' => 'destaque-inicial', 'orderby' => 'rand', 'posts_per_page' => -1 ) );
		            while ( $destaqueInicial->have_posts() ) : $destaqueInicial->the_post();
		            	$fotoMobile = rwmb_meta('Restaurantesdovictor_foto_mob'); 
		            	foreach ($fotoMobile as $fotoMobile ) {
		            		$fotoMobile = $fotoMobile;

		            	}
				?>
				<div class="item " style="background:url(<?php echo $fotoMobile["full_url"];   ?>)">
				
					<!-- DESCRIÇÃO -->
					<div class="textoDescricaoCasa">
						<p><?php echo get_the_title() ?></p>
						<span><?php echo get_the_content() ?> </span>
					</div>

				</div>
				<?php endwhile; wp_reset_query(); ?>
				</div>
			</section>
		</div>

	</div>