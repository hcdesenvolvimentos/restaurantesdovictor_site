<?php

/**

 * Restaurantes do Victor functions and definitions.

 *

 * @link https://developer.wordpress.org/themes/basics/theme-functions/

 *

 * @package Restaurantes_do_Victor

 */



if ( ! function_exists( 'restaurantesdovictor_setup' ) ) :

/**

 * Sets up theme defaults and registers support for various WordPress features.

 *

 * Note that this function is hooked into the after_setup_theme hook, which

 * runs before the init hook. The init hook is too late for some features, such

 * as indicating support for post thumbnails.

 */

function restaurantesdovictor_setup() {

	/*

	 * Make theme available for translation.

	 * Translations can be filed in the /languages/ directory.

	 * If you're building a theme based on Restaurantes do Victor, use a find and replace

	 * to change 'restaurantesdovictor' to the name of your theme in all the template files.

	 */

	load_theme_textdomain( 'restaurantesdovictor', get_template_directory() . '/languages' );



	// Add default posts and comments RSS feed links to head.

	add_theme_support( 'automatic-feed-links' );



	/*

	 * Let WordPress manage the document title.

	 * By adding theme support, we declare that this theme does not use a

	 * hard-coded <title> tag in the document head, and expect WordPress to

	 * provide it for us.

	 */

	add_theme_support( 'title-tag' );



	/*

	 * Enable support for Post Thumbnails on posts and pages.

	 *

	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/

	 */

	add_theme_support( 'post-thumbnails' );



	// This theme uses wp_nav_menu() in one location.

	register_nav_menus( array(

		'primary' => esc_html__( 'Primary', 'restaurantesdovictor' ),

	) );



	/*

	 * Switch default core markup for search form, comment form, and comments

	 * to output valid HTML5.

	 */

	add_theme_support( 'html5', array(

		'search-form',

		'comment-form',

		'comment-list',

		'gallery',

		'caption',

	) );



	/*

	 * Enable support for Post Formats.

	 * See https://developer.wordpress.org/themes/functionality/post-formats/

	 */

	add_theme_support( 'post-formats', array(

		'aside',

		'image',

		'video',

		'quote',

		'link',

	) );



	// Set up the WordPress core custom background feature.

	add_theme_support( 'custom-background', apply_filters( 'restaurantesdovictor_custom_background_args', array(

		'default-color' => 'ffffff',

		'default-image' => '',

	) ) );

}

endif;

add_action( 'after_setup_theme', 'restaurantesdovictor_setup' );



/**

 * Set the content width in pixels, based on the theme's design and stylesheet.

 *

 * Priority 0 to make it available to lower priority callbacks.

 *

 * @global int $content_width

 */

function restaurantesdovictor_content_width() {

	$GLOBALS['content_width'] = apply_filters( 'restaurantesdovictor_content_width', 640 );

}

add_action( 'after_setup_theme', 'restaurantesdovictor_content_width', 0 );



/**

 * Register widget area.

 *

 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar

 */

function restaurantesdovictor_widgets_init() {

	register_sidebar( array(

		'name'          => esc_html__( 'Sidebar', 'restaurantesdovictor' ),

		'id'            => 'sidebar-1',

		'description'   => esc_html__( 'Add widgets here.', 'restaurantesdovictor' ),

		'before_widget' => '<section id="%1$s" class="widget %2$s">',

		'after_widget'  => '</section>',

		'before_title'  => '<h2 class="widget-title">',

		'after_title'   => '</h2>',

	) );

}

add_action( 'widgets_init', 'restaurantesdovictor_widgets_init' );



/**

 * Enqueue scripts and styles.

 */

function restaurantesdovictor_scripts() {



	//FONTS

	wp_enqueue_style( 'google-font', 'https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i|Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i');

 



	//JAVA SCRIPT

	wp_enqueue_script( 'restaurantesdovictor-jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js' );

	wp_enqueue_script( 'restaurantesdovictor-lightbox', get_template_directory_uri() . '/js/jquery.lightbox.min.js' );

	wp_enqueue_script( 'restaurantesdovictor-bootsrap', get_template_directory_uri() . '/js/bootstrap.min.js' );

	wp_enqueue_script( 'restaurantesdovictor-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js' );

	wp_enqueue_script( 'restaurantesdovictor-mask', get_template_directory_uri() . '/js/jquery.mask.min.js' );

	wp_enqueue_script( 'restaurantesdovictor-geral', get_template_directory_uri() . '/js/geral.js' );



	//CSS

	wp_enqueue_style( 'restaurantesdovictor-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');

	wp_enqueue_style( 'restaurantesdovictor-bootstrap-font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css');

	wp_enqueue_style( 'restaurantesdovictor-owlcarousel', get_template_directory_uri() . '/css/owl.carousel.css');

	wp_enqueue_style( 'restaurantesdovictor-animate', get_template_directory_uri() . '/css/animate.css');

	wp_enqueue_style( 'restaurantesdovictor-lightbox', get_template_directory_uri() . '/css/jquery.lightbox.css');	

	wp_enqueue_style( 'restaurantesdovictor-hover', get_template_directory_uri() . '/css/hover.css');

	wp_enqueue_style( 'restaurantesdovictor-novoLayout', get_template_directory_uri() . '/css/novoLayout.css');

	wp_enqueue_style( 'restaurantesdovictor-style', get_stylesheet_uri() );



	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {

		wp_enqueue_script( 'comment-reply' );

	}

}

add_action( 'wp_enqueue_scripts', 'restaurantesdovictor_scripts' );



/**

 * Implement the Custom Header feature.

 */

require get_template_directory() . '/inc/custom-header.php';



/**

 * Custom template tags for this theme.

 */

require get_template_directory() . '/inc/template-tags.php';



/**

 * Custom functions that act independently of the theme templates.

 */

require get_template_directory() . '/inc/extras.php';



/**

 * Customizer additions.

 */

require get_template_directory() . '/inc/customizer.php';



/**

 * Load Jetpack compatibility file.

 */

require get_template_directory() . '/inc/jetpack.php';

if (class_exists('ReduxFramework')) {

	require_once (get_template_directory() . '/redux/sample-config.php');

}



// EXTENSÃO DA FUNÇÂO WP_GET_ARCHIVES

function extensaoFiltrosArchive($where,$args){

	

	$year		= isset($args['year']) 		? $args['year'] 	: '';

	$month		= isset($args['month']) 	? $args['month'] 	: '';

	$monthname	= isset($args['monthname']) ? $args['monthname']: '';

	$day		= isset($args['day']) 		? $args['day'] 		: '';

	$dayname	= isset($args['dayname']) 	? $args['dayname'] 	: '';

	if($year){

		$where .= " AND YEAR(post_date) = '$year' ";

		$where .= $month ? " AND MONTH(post_date) = '$month' " : '';

		$where .= $day ? " AND DAY(post_date) = '$day' " : '';

	}

	if($month){

		$where .= " AND MONTH(post_date) = '$month' ";

		$where .= $day ? " AND DAY(post_date) = '$day' " : '';

	}

	if($monthname){

		$where .= " AND MONTHNAME(post_date) = '$monthname' ";

		$where .= $day ? " AND DAY(post_date) = '$day' " : '';

	}

	if($day){

		$where .= " AND DAY(post_date) = '$day' ";

	}

	if($dayname){

		$where .= " AND DAYNAME(post_date) = '$dayname' ";

	}

	return $where;

}

add_filter( 'getarchives_where', 'extensaoFiltrosArchive',10,2);



function cc_mime_types($mimes) {

  $mimes['svg'] = 'image/svg+xml';

  return $mimes;

}

add_filter('upload_mimes', 'cc_mime_types');



//PAGINAÇÃO

function pagination($pages = '', $range = 4){

    $showitems = ($range * 2)+1;



    global $paged;

    if(empty($paged)) $paged = 1;



    if($pages == '')

    {

        global $wp_query;

        $pages = $wp_query->max_num_pages;

        if(!$pages)

        {

            $pages = 1;

        }

    }



    if(1 != $pages)

    {

        echo "<div class=\"paginador\">";

        //if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";

        //if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";



$htmlPaginas = "";

        for ($i=1; $i <= $pages; $i++)

        {

            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))

            {

                $htmlPaginas .= ($paged == $i)? '<a href="' . get_pagenum_link($i) . '" class="numero selecionado">' . $i . '</a>' : '<a href="' . get_pagenum_link($i) . '" class="numero">' . $i . '</a>';

            }

        }



        if ($paged < $pages && $showitems < $pages) echo '<a href="' . get_pagenum_link($paged + 1) . '" class="esquerda"><i class="fa fa-chevron-left" aria-hidden="true"></a></i></a>';

        echo $htmlPaginas;

       

if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo '<a href="' . get_pagenum_link($pages) . '" class="direita"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>';

        echo "</div>\n";

    }

}



function customExcerpt($qtdCaracteres) {

  $excerpt = get_the_excerpt();

  $qtdCaracteres++;

  if ( mb_strlen( $excerpt ) > $qtdCaracteres ) {

    $subex = mb_substr( $excerpt, 0, $qtdCaracteres - 5 );

    $exwords = explode( ' ', $subex );

    $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );

    if ( $excut < 0 ) {

      echo mb_substr( $subex, 0, $excut );

    } else {

      echo $subex;

    }

    echo '...';

  } else {

    echo $excerpt;

  }

}



// register_nav_menu( 'barDoVictor', __( 'Bar do Victor', 'themify' ) );

// register_nav_menu( 'bistroDoVictor', __( 'Bistrô do Victor', 'themify' ) );

// register_nav_menu( 'barDoVictorEspanha', __( 'Bar do Victor praça espanha', 'themify' ) );

// register_nav_menu( 'petiscariaVictor', __( 'Petiscaria do Victor', 'themify' ) );





function register_my_menus() {

register_nav_menus(

array(

'barDoVictor' => __( 'Bar do Victor' ),

'bistroDoVictor' => __( 'Bistrô do Victor' ),

'barDoVictorEspanha' => __( 'Bar do Victor praça espanha' ),

'petiscariaVictor' => __( 'Petiscaria do Victor' ),

)

);

}

add_action( 'init', 'register_my_menus' );



// VERSIONAMENTO DE FOLHAS DE ESTILO

function my_wp_default_styles($styles)

{

$styles->default_version = "01042018";

}

add_action("wp_default_styles", "my_wp_default_styles");



 	// VERSIONAMENTO DE FOLHAS DE ESTILO

function my_wp_default_scripts($scripts)

{

$scripts->default_version = "01042018";

}

add_action("wp_default_scripts", "my_wp_default_scripts");



