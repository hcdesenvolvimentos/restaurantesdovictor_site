<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Restaurantes_do_Victor
 */
global $post;


$frase = $configuracao['opt-frase-cardapio'];
get_header(); ?>

		<div class="pg pg-cardapio">
	
			<!-- BANNER -->
			<section class="banner" style="background:url(<?php bloginfo('template_directory'); ?>/img/bg-cardapio.png)">
				<h6 class="hidden">Banner Cardápio</h6>
				<p>Cardápio</p>
				<span><?php echo $frase  ?></span>
			</section>

			<div class="container">
				<section class="areaCardapio">
					<h6 class="hidden">Cardápio</h6>
					<div class="row">

						<div class="col-md-3">
							
							<!-- MENU  -->
							<nav class="sidebarCardapio">
								<?php

									// DEFINE A TAXONOMIA
									$taxonomia = 'categoriaPetiscaria';

									// LISTA AS CATEGORIAS PAI DOS SABORES
									$categoriasCardapio = get_terms( $taxonomia, array(
										'orderby'    => 'count',
										'hide_empty' => 0,
										'parent'	 => 0
									));
									
									
									
									foreach ($categoriasCardapio as $categoriaCardapio):								
											$nome = $categoriaCardapio->name;	
											$descricao = $categoriaCardapio->description;
											$categoriaAtivaImg = z_taxonomy_image_url($categoriaCardapio->term_id);
										
								?>
								<a href="<?php echo get_category_link($categoriaCardapio->term_id); ?>">
									<img src="<?php echo $categoriaAtivaImg ?>" alt="" class="img-respnsive">
									<h2><?php echo $nome  ?></h2>
									<i class="fa fa-angle-right" aria-hidden="true"></i>
								</a>

								<?php  endforeach;?>
							</nav>
						
						</div>

						<div class="col-md-9" id="prato">

							<div class="cardapioPratos" id="#prato">
								<div class="pratodestaque" style="background:url(<?php echo $configuracao['opt-cardapioPV']['url']; ?>)"></div>
								<p class="menuChamadaCardapio">Conheça as deliciosas opções de nosso cardápio. Navegue entre as categorias no menu lateral.</p>
								<i class="fa fa-arrow-left iconeChamada"></i>
							</div>

						</div>
					</div>

				</section>	
			</div>	
		
</div>
<script>
// $(window).load(function(){
// $(window).scrollTop(0);
// });
	
// 	window.onload = function () {
		
// 		$('html, body').animate({
// 			scrollTop: $('#prato').offset().top - 120
// 		}, 1000);
// 	}
</script>

<?php
get_footer();
 include (TEMPLATEPATH . '/inc/scriptMapa.php');
