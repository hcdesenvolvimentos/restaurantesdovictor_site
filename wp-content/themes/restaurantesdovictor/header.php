<?php

/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Restaurantes_do_Victor
 */
session_start();

global $configuracao;

// ESTABELECIMENTOS
	$estBVSL 	  	 = 'Bar do Victor';
	$estBVPE 	  	 = 'Bar do Victor &#8211; Praça da Espanha';
	$estPVSF 	 	 = 'Petiscaria do Victor';
	$estBVPB 	  	 = 'Bistrô do Victor';

	$urls 		  	 = array();

	// URLS GERAIS
	// $urls['blog'] 	 = $estBVPE;
	// $urls['eventos']  = $estBVPE;

	// BAR DO VICTOR SÃO LOURENÇO
	$urls['bar-do-victor']								= $estBVSL;
	$urls['quem-somos-bar-do-victor']					= $estBVSL;
	$urls['cardapio']									= $estBVSL;
	$urls['contato-bar-do-victor']						= $estBVSL;
	$urls['clube-bar-do-victor']						= $estBVSL;	
	

	// BAR DO VICTOR PRAÇA DA ESPANHA
	$urls['bar-do-victor-praca-espanha']				= $estBVPE;
	$urls['quem-somos-bar-do-victor-praca-da-espanha'] 	= $estBVPE;
	$urls['cardapio-bar-praca-espanha']					= $estBVPE;
	$urls['clube-bar-do-victor-praca-espanha']			= $estBVPE;
	$urls['contato-bar-do-victor-praca-espanha']		= $estBVPE;


	// PETISCARIA DO VICTOR SANTA FELICIDADE
	$urls['petiscaria-do-victor']						= $estPVSF;
	$urls['quem-somos-petiscaria-do-victor'] 			= $estPVSF;
	$urls['cardapio-petiscaria']						= $estPVSF;
	$urls['clube-petiscaria-do-victor']					= $estPVSF;
	$urls['contato-petiscaria-do-victor']		   		= $estPVSF;
	


	// BISTRÔ DO VICTOR PARKSHOPPING BARIGUI 
	$urls['bistro-do-victor']							= $estBVPB;
	$urls['quem-somos-bistro-do-victor'] 				= $estBVPB;
	$urls['cardapio-bistro']							= $estBVPB;
	$urls['clube-bistro-do-victor']						= $estBVPB;
	$urls['contato-bistro-do-victor']		   			= $estBVPB;
	

	// PATH DA URL REQUISITADA
	$urlPathCompleto 	= $_SERVER['REQUEST_URI'];

	// $urlQtd = count(explode('/',$urlPathCompleto));

	$urlPath 		  	= basename($urlPathCompleto);

	// DEFINIÇÃO DA SESSÂO
	if(isset($urls[$urlPath])){
		$_SESSION['nomeFranquia'] = $urls[$urlPath];
	}else{

		// SE NÃO HOUVER SESSÃO
		if(!isset($_SESSION['nomeFranquia'])){
			$_SESSION['nomeFranquia'] = $estBVPE;
		}

	}

if ($_SESSION['nomeFranquia'] == "Bar do Victor") {								
	$logoFraquias = $configuracao['opt-logo']['url'];
	$slug = "bar-do-victor";
	$cardapioUrl  = "/cardapio/cardapio.php";	
} else if ($_SESSION['nomeFranquia'] == "Bistrô do Victor") {
	$logoFraquias = $configuracao['opt-logo-bistro']['url'];
	$slug = "bistro-do-victor";
	$cardapioUrl  = "/cardapio/cardapio.php";
} else if ($_SESSION['nomeFranquia'] == "Petiscaria do Victor") {
	$logoFraquias = $configuracao['opt-logo-Petiscaria']['url'];
	$slug = "petiscaria-do-victor";
} else if ($_SESSION['nomeFranquia'] == "Bar do Victor &#8211; Praça da Espanha") {
	$logoFraquias = $configuracao['opt-logo-espanha']['url'];
	$slug = "bar-do-victor-praca-espanha";
	$cardapioUrl  = "/cardapio/cardapio.php";
}else{
	$logoFraquias = $configuracao['opt-logo']['url'];
	$slug = "bar-do-victor";
	$cardapioUrl  = "/cardapio/cardapio.php";
}

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<meta property="og:image" content="http:<?php bloginfo('template_directory'); ?>/img/logo-reastaurantesvictor.png">
<meta property="og:image:type" content="image/png">
<meta property="og:image:width" content="800">
<meta property="og:image:height" content="600"> 
<!-- End Facebook Pixel Code -->
<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/fave.ico">

<?php wp_head(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67616006-26', 'auto');
  ga('send', 'pageview');

</script>
<meta name="google-site-verification" content="lLlwYaxcD2p9RlqoxzeUVgLTqBNL0SmDtvyPxslpV8Q" />
<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){
z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='https://v2.zopim.com/?zqxOO9tYhnT2S8L95y2eMbUNNR65iWy1';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zendesk Chat Script-->
</head>

<body <?php body_class(); ?>>
<?php if ($cardapioUrl) {include (TEMPLATEPATH . "$cardapioUrl");}; ?>

<script type="text/javascript" async src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/8c4b6930-2a4d-4002-aaea-63e21fd27dba-loader.js"></script>
	<!-- TOPO -->
	<header class="topo">

		<div class="area-menu">
			<div class="row">
				<div class="col-md-2 logo">
					<a href="<?php echo home_url('/'); ?>" title="<?php echo $_SESSION['nomeFranquia'] ?>">
						<strong style="background:url(<?php echo $logoFraquias ?>)">Bar do Victor</strong>
					</a>
				</div>
				<div class="col-md-10">
					<!-- MENU  -->		
					<div class="navbar" role="navigation">	
										
						<!-- MENU MOBILE TRIGGER -->
						<button type="button" id="botao-menu" class="navbar-toggle collapsed hvr-pop" data-toggle="collapse" data-target="#collapse">
							<span class="sr-only"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<!--  MENU MOBILE-->
						<div class="row navbar-header">			

							<nav class="collapse navbar-collapse" id="collapse">

								<ul class="nav navbar-nav" style="display:none">			
									
									<li class="dropdown">
										<a href="#" class="hvr-float-shadow dropdown-toggle" data-toggle="dropdown" id="embarcacoes" aria-haspopup="true" aria-expanded="false" title="Embarcações">Embarcações<i class="fa fa-chevron-down" aria-hidden="true"></i></a>
										<ul class="dropdown-menu">
											<li>

												<a href="<?php echo home_url('bar-do-victor/'); ?>" title="Bar do Victor São Lourenço" id="barVictor">Bar do Victor<span>São Lourenço</span></a>
												<a href="#" title="Bar do Victor Santa Felicidade" id="barVictorespanha">Bar do Victor<span>Praça Espanha</span></a>
												<a href="#" title="" id="petiscaria">Petiscaria do Victor<span>Santa Felicidade</span></a>
												<a href="#" title="" id="bistroVictor">Bistrô do Victor<span>Shopping Barigui</span></a>
												<a href="#" title="" id="fish">Fish 'n' Chips<span>Shopping Mueeler</span></a>
												<a href="#" title="" class="ultimo-item" id="restaurante">Restaurante do Victor<span>Shopping Mueeler</span></a>											
																					
											</li>																																					
										</ul>							
									</li>
									<li><a href="<?php echo home_url('/quem-somos/'); ?>" class="hvr-float-shadow" id="quemSomos" title="Quem Somos">Quem Somos</a></li>						
									<li><a href="<?php echo home_url('/cardapio/'); ?>" class="hvr-float-shadow" id="cardapio" title="Cardápio">Cardápio</a></li>
									<li><a href="<?php echo home_url('/eventos/'); ?>" class="hvr-float-shadow" id="eventos" title="Eventos">Eventos</a></li>
									<li><a href="<?php echo home_url('/clube/'); ?>" class="hvr-float-shadow" id="clube" title="Clube">Clube</a></li>																			
									<li><a href="<?php echo home_url('blog/'); ?>" class="hvr-float-shadow" id="blog" title="Blog">Blog</a></li>																			
									<li><a href="<?php echo home_url('/contato/'); ?>" id="contato" class="hvr-float-shadow" title="Contato">Contato</a></li>																			
									
								</ul>
								

								<script>
									$(document).ready(function(){

										$('.sub-menu').addClass( "dropdown-menu" );

									});
								</script>
								<?php 
								if ($_SESSION['nomeFranquia'] == "Bar do Victor") {
									$barDoVictor = array(
										'theme_location'  => '',
										'menu'            => 'Bar do Victor',
										'container'       => false,
										'container_class' => '',
										'container_id'    => '',
										'menu_class'      => 'nav navbar-nav',
										'menu_id'         => '',
										'echo'            => true,
										'fallback_cb'     => 'wp_page_menu',
										'before'          => '',
										'after'           => '',
										'link_before'     => '',
										'link_after'      => '',
										'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
										'depth'           => 2,
										'walker'          => ''
										);
									wp_nav_menu( $barDoVictor );
									
								} else if ($_SESSION['nomeFranquia'] == "Bistrô do Victor") {
									$bistroDoVictor = array(
										'theme_location'  => '',
										'menu'            => 'Bistrô do Victor',
										'container'       => false,
										'container_class' => '',
										'container_id'    => '',
										'menu_class'      => 'nav navbar-nav',
										'menu_id'         => '',
										'echo'            => true,
										'fallback_cb'     => 'wp_page_menu',
										'before'          => '',
										'after'           => '',
										'link_before'     => '',
										'link_after'      => '',
										'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
										'depth'           => 2,
										'walker'          => ''
										);
									wp_nav_menu( $bistroDoVictor );
									
								} else if ($_SESSION['nomeFranquia'] == "Petiscaria do Victor") {
									$petiscariaVictor = array(
										'theme_location'  => '',
										'menu'            => 'Petiscaria do Victor',
										'container'       => false,
										'container_class' => '',
										'container_id'    => '',
										'menu_class'      => 'nav navbar-nav',
										'menu_id'         => '',
										'echo'            => true,
										'fallback_cb'     => 'wp_page_menu',
										'before'          => '',
										'after'           => '',
										'link_before'     => '',
										'link_after'      => '',
										'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
										'depth'           => 2,
										'walker'          => ''
										);
									wp_nav_menu( $petiscariaVictor );
									
								} else if ($_SESSION['nomeFranquia'] == "Bar do Victor &#8211; Praça da Espanha") {
									$barDoVictorEspanha = array(
										'theme_location'  => '',
										'menu'            => 'Bar do Victor praça espanha',
										'container'       => false,
										'container_class' => '',
										'container_id'    => '',
										'menu_class'      => 'nav navbar-nav',
										'menu_id'         => '',
										'echo'            => true,
										'fallback_cb'     => 'wp_page_menu',
										'before'          => '',
										'after'           => '',
										'link_before'     => '',
										'link_after'      => '',
										'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
										'depth'           => 2,
										'walker'          => ''
										);
									wp_nav_menu( $barDoVictorEspanha );
									
								}else{
									$barDoVictor = array(
										'theme_location'  => '',
										'menu'            => 'Bar do Victor',
										'container'       => false,
										'container_class' => '',
										'container_id'    => '',
										'menu_class'      => 'nav navbar-nav',
										'menu_id'         => '',
										'echo'            => true,
										'fallback_cb'     => 'wp_page_menu',
										'before'          => '',
										'after'           => '',
										'link_before'     => '',
										'link_after'      => '',
										'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
										'depth'           => 2,
										'walker'          => ''
										);
									wp_nav_menu( $barDoVictor );
								}

								

								
								
						     	?>
							</nav>						

						</div>			
						
					</div>
				</div>
			</div>	
		</div>
	
	</header>

	
	