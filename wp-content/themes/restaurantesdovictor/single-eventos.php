<?php
session_start();
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Restaurantes_do_Victor
 */
$fotoEvento = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$fotoEvento = $fotoEvento[0];	
$subtitulo =  rwmb_meta('Restaurantesdovictor_evento_subtitulo');
$casasId = rwmb_meta('Restaurantesdovictor_evento_casas');
$pratosId = rwmb_meta('Restaurantesdovictor_evento_pratos');

// DEFINE O ESTABELECIMENTO BASEADO NA CATEGORIA DO EVENTO
	$categoriaEvento 	 = $term_list = wp_get_post_terms($post->ID, 'categoriaEvento', array("fields" => "all"));
	$categoriaEventoNome = $categoriaEvento[0]->name;

	$_SESSION["nomeFranquia"]= $categoriaEventoNome;

get_header(); ?>
<!-- VERIFICAÇÃO PARA RECEBER TAXONOMYA DE CADA FRANQUIA -->
<?php 
	if ($_SESSION['nomeFranquia'] == "Bar do Victor") {	
		
		$formulario =  $configuracao['opt-formulario-eventos'];
		

	} else if ($_SESSION['nomeFranquia'] == "Bistrô do Victor") {
	
		$formulario = $configuracao['opt-formulario-eventos-bistro'];
		

	} else if ($_SESSION['nomeFranquia'] == "Petiscaria do Victor") {
	
		$formulario =  $configuracao['opt-formulario-eventos-Petiscaria'];
		

	} else if ($_SESSION['nomeFranquia'] == "Bar do Victor praça espanha") {
		
		$formulario = $configuracao['opt-formulario-eventos-espanha'];
		

	}
?>
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      
	       <div class="modal-body">
	      	    <button type="button" class="close" data-dismiss="modal">&times;</button>
	  		<div class="pg pg-contato">
				
				<p class="subtitulo-contato text-center">SOLICITAR RESERVA</p>							

				<div class="formulario-contato">
				
					<iframe src="http://waitlist.tagme.com.br/widget/56c776f10896b3cd13c600c8" frameBorder="0" scrolling="auto" width="100%" height="563">   Desculpe o seu navegador não suporta iframes. </iframe>

				</div>
			
			</div>

	      </div>
	      
	    </div>
	  </div>
	</div>

	<div class="pg pg-evento" style="display:none">
		
		<div class="banner-evento" style="background:url(<?php bloginfo('template_directory')?>/img/foto-detalhe-evento.png)">
			<div class="nome-evento">
				<h3 id="nomeEvento"><?php echo get_the_title() ?></h3>
				<p> <?php echo $subtitulo  ?></p>
			</div>
		</div>

		<section class="sobre-evento">
			<div class="row">
				<div class="col-md-6">
					<div class="texto-sobre-evento">
						<?php echo the_content() ?>
					</div>
					<div class="foto-sobre-evento" style="background:url(<?php echo $fotoEvento ?>)"></div>
				</div>
				<div class="col-md-6">
					<div class="casas-participantes-evento">

						<a href="#myModal" data-toggle="modal" data-target="#myModal" class="faca-parte">Faça sua reserva aqui!</a>
						<span>Casas Participantes</span>
						<ul>

							
						<?php
						foreach ($casasId as $casasId):
								$idCasa = $casasId;
							
							
							// LOOP DE ESTABELECIMENTO
							$Eventos = new WP_Query( array( 'post_type' => 'estabelecimento', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
							while ( $Eventos->have_posts() ) : $Eventos->the_post();
								$logo = rwmb_meta('Restaurantesdovictor_logo_estabelecimento');
								$link = rwmb_meta('Restaurantesdovictor_link_estabelecimento'); 
								$postID = $post->ID;

								if ($postID == $idCasa):
									
						?>
							
							<li class="hvr-float">
								<?php 
									foreach ($logo as $logo):						
										?>
									<!-- LOGO -->
									<a href="<?php echo $link ?>" target="_blank"><img src="<?php echo $logo['full_url'] ?>" alt="<?php echo get_the_title() ?>" class="img-responsive"></a>
								<?php endforeach; ?>
							</li>
							
						<?php  endif;endwhile;wp_reset_query(); endforeach?>
						



						</ul>

						<!-- <div class="icon"><img src="<?php bloginfo('template_directory'); ?>/img/iconCasa.png" class="img-responsive" alt=""></div> -->
						
					</div>
				</div>
			</div>
		</section>

		<section class="carrossel-pratos-eventos">
			
			<!-- TÍTULO PÁGINA  -->
			<div class="titulo-paginas">
				<p>Pratos</p>
			</div>	
	
			<!-- BOTÕES -->
			<button class="navegacaoPratosEventosFrent hvr-bounce-to-left hidden-xs"><i class="fa fa-angle-left"></i></button>
			<button class="navegacaoPratosEventosTras hvr-bounce-to-right hidden-xs"><i class="fa fa-angle-right"></i></button>
			<!-- CARROSSEL DE FOTOS DESTAQUES -->			
			<div id="carrosselPratoseventos" class="owl-Carousel">
			
				<?php
					foreach ($pratosId as $pratosId):
						$idprato = $pratosId;
					
					
					// LOOP DE CARDÁPIO
					$cardapio = new WP_Query( array( 'post_type' => 'cardapio', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
					while ( $cardapio->have_posts() ) : $cardapio->the_post();
						$fotocardapio = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotocardapio = $fotocardapio[0];	
						$postIDcardapio = $post->ID;
						$Subtitulo = rwmb_meta('Restaurantesdovictor_prato_Subtitulo');
						if ($postIDcardapio == $idprato):
							
				?>
				<!-- ITEM DESTAQUE   -->
				<a href="<?php echo get_permalink(); ?>" class="item">
					<div class="foto-Pratoseventos" style="background:url(<?php echo $fotocardapio ?>)">
						<div class="nome-funcao-Pratoseventos">
						
							<span><?php echo get_the_title() ?> </span>
						</div>
					</div>
					<p class="descricao"><?php echo $Subtitulo ?> </p>
				</a>

				<?php  endif; endwhile;wp_reset_query(); endforeach?>
			</div>
			

		</section>

	</div>

<?php

get_footer();
 include (TEMPLATEPATH . '/inc/scriptMapa.php');
