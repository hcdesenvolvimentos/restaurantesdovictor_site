<?php
session_start();
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Restaurantes_do_Victor
 */


get_header(); ?>

<!-- VERIFICAÇÃO PARA RECEBER TAXONOMYA DE CADA FRANQUIA -->
<?php 
		if ($_SESSION['nomeFranquia'] == "Bar do Victor") {	

		$taxonomia = 'bar-do-victor-sao-lourenco';
		$taxonomiaIntegrantes = 'bar-do-victor-sao-lourenco';
		$estabelecimento = $configuracao['opt-nome-estabelecimento'];
		$endereco = $configuracao['opt-endereco'];
		$corCasa = "#ffcb04";
		

	} else if ($_SESSION['nomeFranquia'] == "Bistrô do Victor") {

		$taxonomia = 'bistro-do-victor';
		$taxonomiaIntegrantes = 'bistro-do-victor';
		$endereco = $configuracao['opt-endereco-bistro'];
		$estabelecimento = $configuracao['opt-nome-estabelecimento-bistro'];
		$corCasa = "#fa8621";

	} else if ($_SESSION['nomeFranquia'] == "Petiscaria do Victor") {
		$endereco = $configuracao['opt-endereco-Petiscaria'];
		$taxonomia = 'petiscaria-do-victor';
		$taxonomiaIntegrantes = 'petiscaria-do-victor';
		$estabelecimento = $configuracao['opt-nome-estabelecimento-Petiscaria'];
		$corCasa = "#8d2a2f";
		$color = "#fff";
		

	} else if ($_SESSION['nomeFranquia'] == "Bar do Victor &#8211; Praça da Espanha") {

		$taxonomia = 'bar-do-victor-praca-da-espanha';
		$taxonomiaIntegrantes = 'bar-do-victor-praca-da-espanha';
		$endereco = $configuracao['opt-endereco-espanha'];
		$estabelecimento = $configuracao['opt-nome-estabelecimento-espanha'];
		$corCasa = "#56adb5";
		

	}
?>

<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      
	       <div class="modal-body">
	      	    <button type="button" class="close" data-dismiss="modal">&times;</button>
	  		<div class="pg pg-contato">
				
				<p class="subtitulo-contato text-center">FORMULÁRIO DE CONTATO RESERVA</p>							

				<div class="formulario-contato">

				<iframe src="http://waitlist.tagme.com.br/widget/56c776fe0896b3cd13c60127" frameBorder="0" scrolling="auto" width="100%" height="563">   Desculpe o seu navegador não suporta iframes. </iframe>

				</div>
			
			</div>

	      </div>
	      
	    </div>
	  </div>
	</div>
<div class="pg pg-eventos" style="display:" >
	<section class="area-carrossel-eventos">
		<!-- CARROSSEL DE FOTOS DESTAQUES -->
		
			<!-- BOTÕES -->
			<button class="navegacaoEventosFrentPG hvr-bounce-to-right"><i class="fa fa-angle-left"></i></button>
			<button class="navegacaoEventosTrasPG hvr-bounce-to-left"><i class="fa fa-angle-right"></i></button>

			<div id="carrossel-eventos" class="owl-Carousel">
			
			<?php 
					
				// LOOP DE DESTAQUES					
				$eventos = new WP_Query(array(
					'post_type'     => 'destaque',
					'posts_per_page'   => -1,
					'tax_query'     => array(
						array(
							'taxonomy' => 'categoriaDestaque',
							'field'    => 'slug',
							'terms'    => $taxonomia ,
							)
						)
					)
				);

	            while ( $eventos->have_posts() ) : $eventos->the_post();				
	            $fotocardapio = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	            $fotocardapio = $fotocardapio[0];
				$subtitulo =  rwmb_meta('Restaurantesdovictor_evento_subtitulo');
				$casasId = rwmb_meta('Restaurantesdovictor_evento_casas');
				$pratosId = rwmb_meta('Restaurantesdovictor_evento_pratos');
				$data = rwmb_meta('Restaurantesdovictor_evento_data');
	            	
		
			?>	
				<!-- ITEM DESTAQUE   -->
				<div class="item" style="background:url(<?php echo $fotocardapio ?>)">
					 
						<div class="foto-evento"  >
							<div class="area-informacoes-evento" style="display:none">
								<h3><?php echo get_the_title() ?></h3>
								<span><?php echo $data  ?></span>
								<p><?php customExcerpt(300); ?></p>
								<div class="text-center">
									<a href="<?php echo $Subtitulo = rwmb_meta('Restaurantesdovictor_evento_linkEvento'); ?>">Saiba mais!</a>
									<a   href="#myModal"  data-toggle="modal" data-target="#myModal"  id="btnEventos"  >Reservas!</a>
								</div>
							</div>
						</div>
							<!-- CASAS PARTICIPANTES -->
						<div class="casas-participantes-evento" style="display:none;">
							<span>Casas Participantes</span>
							<ul>
								<?php
									foreach ($casasId as $casasId):
											$idCasa = $casasId;
										
										
										// LOOP DE CLIENTES PARA PEGAR NOME DO CLIENTE
										$Eventos = new WP_Query( array( 'post_type' => 'estabelecimento', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
										while ( $Eventos->have_posts() ) : $Eventos->the_post();
											$logo = rwmb_meta('Restaurantesdovictor_logo_estabelecimento');
											$link = rwmb_meta('Restaurantesdovictor_link_estabelecimento'); 
											$postID = $post->ID;
											
											if ($postID == $idCasa):
												
								?>
								<li title="<?php echo get_the_title() ?>" class="hvr-float">
									<?php foreach ($logo as $logo): ?>
									<a href="<?php echo $link ?>" target="_blank" alt="<?php echo get_the_title() ?>"><img src="<?php echo $logo['full_url'] ?>" alt="<?php echo get_the_title() ?>" class="img-responsive"></a>
									<?php endforeach ?>
								</li>
								<?php  endif;endwhile;wp_reset_query(); endforeach?>
							</ul>				
						</div>
					</div>


			<?php endwhile; wp_reset_query(); ?>
			</div>
		

		
	</section>

	<!-- ÁREA PRÓXIMOS EVENTOS -->
		<section class="area-proximos-eventos-inicial" style="display:">
			
			<!-- TÍTULO PÁGINA  -->
			<div class="titulo-paginas">
				<p>Próximos eventos</p>
			</div>	

			<?php 
				// LOOP DE EVENTOS					
				$eventos = new WP_Query(array(
					'post_type'     => 'eventos',
					'posts_per_page'   => -1,
					'tax_query'     => array(
						array(
							'taxonomy' => 'categoriaEvento',
							'field'    => 'slug',
							'terms'    => $taxonomia ,
							)
						)
					)
				);
				if ($eventos->have_posts()) {
					
				
			?>
		
			<!-- CARROSSEL DE FOTOS DESTAQUES -->
			<div class="carrossel-eventos-inicial">	

				<!-- BOTÕES -->
				<button class="navegacaoEventosFrent"><i class="fa fa-angle-left"></i></button>
				<button class="navegacaoEventosTras"><i class="fa fa-angle-right"></i></button>

				<div id="carrossel-eventos-inicial" class="owl-Carousel">
					
					<?php 

				
						while ( $eventos->have_posts() ) : $eventos->the_post();

						$fotoEvento = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoEvento = $fotoEvento[0];	
						$subtitulo =  rwmb_meta('Restaurantesdovictor_evento_subtitulo');	
						$img =  rwmb_meta('Restaurantesdovictor_img_linkEvento');
							
						foreach ($img as $img) {
							$img = $img['full_url'];
							
							}	

					?>
					<!-- ITEM DESTAQUE   -->
					<div class="desktop item" style="background:url(<?php echo $img  ?>)">
						<img src="<?php echo $fotoEvento ?>" class="img-responsivo">
					</div>
  

					<?php endwhile; wp_reset_query(); ?>

				</div>

			</div>
				<div class="text-center">
						<a  href="<?php echo $Subtitulo = rwmb_meta('Restaurantesdovictor_evento_linkEvento'); ?>" style="background:<?php echo $corCasa ?>; color:<?php echo $color ?>">Mais informações</a>
						<a  href="#myModal"  data-toggle="modal" data-target="#myModal"  id="btnEventos"  style="background:<?php echo $corCasa ?>; color:<?php echo $color ?>" >Reservas</a>
					</div>
			<?php } ?>
		</section>	
</div>		

<?php

get_footer();
 include (TEMPLATEPATH . '/inc/scriptMapa.php');

