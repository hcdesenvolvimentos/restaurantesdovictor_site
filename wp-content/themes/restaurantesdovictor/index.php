<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Restaurantes_do_Victor
 */

get_header(); ?>

<!-- FONTS -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

<!-- CSS -->
<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/blog/carrossel/slick/slick.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/blog/carrossel/slick/slick-theme.css">
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/blog/css/site.css" />




<meta property="og:image" content="http:<?php bloginfo('template_directory'); ?>/img/logo-reastaurantesvictor.png">
<meta property="og:image:type" content="image/png">
<meta property="og:image:width" content="800">
<meta property="og:image:height" content="600"> 
	
	<div class="pg pg-inicialBlog">
			
			<!-- CADASTRE-SE -->
			<section class="newllslatter">	
				<div class="row">	
					<div class="col-sm-4">
						<div class="info">	
							<p>Assine nossa newsletter e recebabr	<br>novidades do mundo gastronômico!</p>
						</div>
					</div>
					<div class="col-sm-8">	
						<div class="formNews">	
							<input type="text" placeholder=Nome>
							<input type="text" placeholder="E-mail">
							<input type="submit" value="Enviar">
						</div>

					</div>
				</div>
			</section>
			
			<!-- CARROSSEL -->
			<div class="containerCorrecaoX">
				<section class="center slider">
					<?php 
						$i = 0;
						// LOOP DE ESTABELECIMENTO
						$posts = new WP_Query( array( 'post_type' => 'post', 'orderby' => 'asc', 'posts_per_page' => -1 ) );
					    while ( $posts->have_posts() ) : $posts->the_post();
							// FOTO DESTACADA
							$fotoDestaquePost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$fotoDestaquePost = $fotoDestaquePost[0];
							$post_destaque = rwmb_meta('Restaurantesdovictor_post_destaque'); 
							if ($post_destaque == "1"):
								
						
							
					?>	
					<div>
						<img src="<?php echo $fotoDestaquePost ?>" alt="<?php echo get_the_title() ?> ">
					</div>
					<?php endif;$i++; endwhile; wp_reset_query(); ?>

				</section>
			</div>
			
			<section class="sessaoCategorias">
				<div class="container">
					<div class="tituloBlog">
						<h6>Categorias</h6>
					</div>

					<!-- CARROSSEL DESTAQUE -->
					<div class="carrosselCategoriaBlog" id="carrosselCategoriaBlog">
						<?php

							// CATEGORIA ATUAL
							$categoriaAtual = get_the_category();
							$categoriaAtual = $categoriaAtual[0]->cat_name;

							// LISTA DE CATEGORIAS
							$arrayCategorias = array();
							$categorias=get_categories($args);
							foreach($categorias as $categoria):
							$arrayCategorias[$categoria->cat_ID] = $categoria->name;
							$nomeCategoria = $arrayCategorias[$categoria->cat_ID];
							$categoriaAtivaImg = z_taxonomy_image_url($categoria->cat_ID);

							if ($categoriaAtivaImg != ""):
								
							
						?>
						<div class="item">
							<figure>
								<a href="<?php echo get_category_link($categoria->cat_ID); ?>">
									<img src="<?php echo $categoriaAtivaImg ?>" alt="<?php echo $nomeCategoria ?>" class="timed-animation">
								</a>
							</figure>
						</div>
						<?php endif;endforeach; ?>
					

					</div>
					
				</div>
			</section>
	
			<section class="ultimasPublicacoes">
				<div class="container">
					<div class="tituloBlog">
						<h6>Últimas Publicações</h6>
					</div>


						<div class="postHomeBlog">
							<ul>
								<?php 
								$posts = new WP_Query( array( 'post_type' => 'post', 'orderby' => 'asc', 'posts_per_page' => 3 ) );
								while ( $posts->have_posts() ) : $posts->the_post();
									$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
									$foto = $foto[0];
									global $post;
									$categories = get_the_category();
										
										
								?>
								<li>
									<a href="<?php echo get_permalink(); ?>">
										<img src="<?php echo $foto ?>" alt="<?php echo get_the_title() ?>">
										<div class="descricao">
											<span><?php echo $categories[0]->name ?></span>
											<h2><?php echo get_the_title() ?></h2>
											<p><?php customExcerpt(80); ?></p>
										</div>
									</a>
								</li>
								<?php endwhile; ?>
							</ul>
						
						<div class="verMais">
							<a href="#">Ver mais</a>
							<?php //if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
						</div>
						
					</div>
				</div>
			</section>


			<section class="areaCategoria">
				<div class="container">

					<nav>
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
							<?php

								
								$i = 0;
								// LISTA DE CATEGORIAS
								$arrayCategorias = array();
								$categorias=get_categories($args);
								foreach($categorias as $categoria):
								$arrayCategorias[$categoria->cat_ID] = $categoria->name;
								$nomeCategoria = $arrayCategorias[$categoria->cat_ID];
								$categoriaAtivaImg = z_taxonomy_image_url($categoria->cat_ID);

								if ($i == 0):
									$ativo = "active";
									
							?>
							<a class="nav-item nav-link <?php echo $ativo ?>" id="nav-<?php echo $categoria->cat_ID ?>-tab" data-toggle="tab" href="#nav-<?php echo $categoria->cat_ID ?>" role="tab" aria-controls="nav-<?php echo $categoria->cat_ID ?>" aria-selected="true"><h3>#<?php echo $nomeCategoria ?> </h3></a>
							<?php else: ?>
							<a class="nav-item nav-link" id="nav-<?php echo $categoria->cat_ID ?>-tab" data-toggle="tab" href="#nav-<?php echo $categoria->cat_ID ?>" role="tab" aria-controls="nav-<?php echo $categoria->cat_ID ?>" aria-selected="true"><h3>#<?php echo $nomeCategoria ?> </h3></a>
							<?php endif;$i++;endforeach; ?>

						</div>
					</nav>

					<div class="tab-content" id="nav-tabContent">
						<?php

							$j = 0;

							// LISTA DE CATEGORIAS
							$arrayCategorias = array();
							$categorias=get_categories($args);
							foreach($categorias as $categoria):
							$arrayCategorias[$categoria->cat_ID] = $categoria->name;
							
							$arrayCategoriasSlug[$categoria->cat_ID] = $categoria->slug;


							$nomeCategoria = $arrayCategorias[$categoria->cat_ID];
							$slug = $arrayCategoriasSlug[$categoria->cat_ID];

							if ($j == 0):
								$ativo = "active";

						?>
						<div class="tab-pane fade <?php echo $ativo ?> in" id="nav-<?php echo $categoria->cat_ID ?>" role="tabpanel" aria-labelledby="nav-<?php echo $categoria->cat_ID ?>-tab">
							<div class="postHomeBlog">
		
								<ul>
									<?php 
										//LOOP DE POST CATEGORIA DESTAQUE				
										$postsCategory = new WP_Query(array(
											'post_type'     => 'post',
											'posts_per_page'   => 3,
											'tax_query'     => array(
												array(
													'taxonomy' => 'category',
													'field'    => 'slug',
													'terms'    => $slug,
													)
												)
											)
										);

										
										// LOOP DE POST
										while ( $postsCategory->have_posts() ) : $postsCategory->the_post();
											//FOTO DESTACADA
											$fotoPostCategory = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
											$fotoPostCategory = $fotoPostCategory[0];
											global $post;
											$categories = get_the_category();
									?>
									<li>
										<a href="">
											<img src="<?php echo $fotoPostCategory ?>" alt="<?php echo get_the_title() ?>">
											<div class="descricao">
												<span><?php echo $categories[0]->name ?></span>
												<h2><?php echo get_the_title() ?></h2>
												<p><?php customExcerpt(80); ?></p>
											</div>
										</a>
									</li>
								<?php  endwhile; wp_reset_query();  ?>
								</ul>
								<div class="verMais">
									<a href="<?php echo get_category_link($categoria->cat_ID); ?>">Ver mais</a>
								</div>
							</div>
						</div>
						<?php else: ?>
						<div class="tab-pane fade" id="nav-<?php echo $categoria->cat_ID ?>" role="tabpanel" aria-labelledby="nav-<?php echo $categoria->cat_ID ?>-tab">
							<div class="postHomeBlog">
		
								<ul>
									<?php 
										//LOOP DE POST CATEGORIA DESTAQUE				
										$postsCategory = new WP_Query(array(
											'post_type'     => 'post',
											'posts_per_page'   => 3,
											'tax_query'     => array(
												array(
													'taxonomy' => 'category',
													'field'    => 'slug',
													'terms'    => $slug,
													)
												)
											)
										);

										
										// LOOP DE POST
										while ( $postsCategory->have_posts() ) : $postsCategory->the_post();
											//FOTO DESTACADA
											$fotoPostCategory = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
											$fotoPostCategory = $fotoPostCategory[0];
											global $post;
											$categories = get_the_category();
									?>
									<li>
										<a href="">
											<img src="<?php echo $fotoPostCategory ?>" alt="<?php echo get_the_title() ?>">
											<div class="descricao">
												<span><?php echo $categories[0]->name ?></span>
												<h2><?php echo get_the_title() ?></h2>
												<p><?php customExcerpt(80); ?></p>
											</div>
										</a>
									</li>
								<?php  endwhile; wp_reset_query();  ?>
								</ul>
								
								<div class="verMais">
									<a href="<?php echo get_category_link($categoria->cat_ID); ?>">Ver mais</a>
								</div>
							</div>
						</div>
						<?php endif;$j++;endforeach; ?>
					</div>

				</div>

				
			</section>

			<!-- CARROSSEL -->
			<div class="containerCorrecaoX">
				<section class="center slider">
					<?php 
						$i = 0;
						// LOOP DE ESTABELECIMENTO
						$posts = new WP_Query( array( 'post_type' => 'post', 'orderby' => 'rand', 'posts_per_page' => -1 ) );
					    while ( $posts->have_posts() ) : $posts->the_post();
							// FOTO DESTACADA
							$fotoDestaquePost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$fotoDestaquePost = $fotoDestaquePost[0];
							$post_destaque = rwmb_meta('Restaurantesdovictor_post_destaque'); 
							if ($post_destaque == "1"):
								
						
							
					?>	
					<div>
						<img src="<?php echo $fotoDestaquePost ?>" alt="<?php echo get_the_title() ?> ">
					</div>
					<?php endif;$i++; endwhile; wp_reset_query(); ?>

				</section>
			</div>

			<section class="newllslatterBottom">
				<div class="container">
					<p>Quer receber novidades do mundo gastronômico, receitas e promoções? Assine nossa newsletter!</p>
					<div class="formNews">	
						<input type="text" placeholder=Nome>
						<input type="text" placeholder="E-mail">
						<input type="submit" value="Enviar">
					</div>
				</div>
			</section>

	</div>

	<!-- FOOTER-->
		<footer class="rodapeBlog">
			
				<div class="row">
					<div class="col-md-3">
						<div class="areaLogo">
							<div class="logo">
								<img src="img/Logo-ReastaurantesVictor.png" alt="">
							</div>
							<div class="descricao">
								<p>Os melhores sabores do mar <strong> desde 1969</strong></p>
							</div>
							<div class="redesSociais">
								<a href="">
									<div class="icone1">
										<i class="fab fa-instagram"></i>
									</div>
								</a>	
								<a href="">
									<div class="icone1">
										<i class="fab fa-facebook-f"></i>
									</div>
								</a>
							    <a href="">
									<div class="icone1">
										<i class="fab fa-linkedin-in"></i>
									</div>
								</a>
								<a href="">
									<div class="icone1">
										<i class="fab fa-youtube"></i>
									</div>
								</a>
							</div>

						</div>
					</div>
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-5">
								<div class="menuRodape">
									<div class="row">
										<div class="col-sm-6 text-center">
											<div class="nav">
												<strong>Restaurante Victor</strong>
												<ul>
													<li>
														<a href="">Quem Somos</a>
													</li>
													<li>
														<a href="">Cardápio</a>
													</li>
													<li>
														<a href="">Reservas</a>
													</li>
													<li>
														<a href="">Delivey</a>
													</li>
													<li>
														<a href="">Eventos</a>
													</li>
													<li>
														<a href="">Blog</a>
													</li>													
												</ul>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="nav">
												<strong> Sabores do Mar</strong>
												<ul>
													<li>
														<a href="">Restaurante pelo Mundo</a>
													</li>
													<li>
														<a href="">Harmonização</a>
													</li>
													<li>
														<a href="">Guia de Vinhos</a>
													</li>
													<li>
														<a href="">Rceitas Victor</a>
													</li>
													<li>
														<a href=""> Calendário</a>
													</li>
													<li>
														<a href="">Saiu na Midia</a>
													</li>													
												</ul>
											</div>
										</div>								
									
									</div>
								</div>
							</div>
							<div class="col-md-7
							">
								<div class="menuRodape">
									<div class="row">
										<div class="col-sm-6">
											<div class="nav">
												<strong>São Lourenço</strong>
												<ul>
													<li>
														<a href=""><i class="fas fa-home"></i> R. Livio Moreira,284</a>
													</li>
													<li>
														<a href=""><i class="fas fa-phone"></i> 041 3353-1920</a>
													</li>
													<li class="hoverHorarios">
														<a href=""><i class="far fa-clock"></i> Horarios</a>
														<div class="horarios">	
															<strong>Almoço/Jantar</strong>
															<span>Segunda e terça: 11:30h ás 23h</span>
															<span>Quarta e sexta: 11:30h ás 00h</span>
															<span>Sábado: 12h à 00h</span>
															<span>Domingo: 12h ás 22h</span>
														</div>
													</li>
													<li>
														<a href=""><i class="far fa-envelope"></i> E-mail</a>
													</li>													
												</ul>
												<strong>Praça Lourenço</strong>
												<ul>
													<li>
														<a href=""><i class="fas fa-home"></i> R. Saldanha Marinho, 1650</a>
													</li>
													<li>
														<a href=""><i class="fas fa-phone"></i> 041 3076-0720</a>
													</li>
													<li class="hoverHorarios">
														<a href=""><i class="far fa-clock"></i> Horários</a>
														<div class="horarios">	
															<strong>Almoço/Jantar</strong>
															<span>Segunda e terça: 11:30h ás 23h</span>
															<span>Quarta e sexta: 11:30h ás 00h</span>
															<span>Sábado: 12h à 00h</span>
															<span>Domingo: 12h ás 22h</span>
														</div>
													</li>
													<li>
														<a href=""><i class="far fa-envelope"></i> E-mail</a>
													</li>													
												</ul>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="nav">
												<strong>ParkShoppingBarigui</strong>
												<ul>
													<li>
														<a href=""><i class="fas fa-home"></i> R. Livio Moreira,284</a>
													</li>
													<li>
														<a href=""><i class="fas fa-phone"></i> 041 3353-1920</a>
													</li>
													<li class="hoverHorarios">
														<a href=""><i class="far fa-clock"></i> Horarios</a>
														<div class="horarios">	
															<strong>Almoço/Jantar</strong>
															<span>Segunda e terça: 11:30h ás 23h</span>
															<span>Quarta e sexta: 11:30h ás 00h</span>
															<span>Sábado: 12h à 00h</span>
															<span>Domingo: 12h ás 22h</span>
														</div>
													</li>
													<li>
														<a href=""><i class="far fa-envelope"></i> E-mail</a>
													</li>													
												</ul>
												<strong>Petiscaria -Sta.Felicidade</strong>
												<ul>
													<li>
														<a href=""><i class="fas fa-home"></i> R. Livio Moreira,284</a>
													</li>
													<li>
														<a href=""><i class="fas fa-phone"></i> 041 3353-1920</a>
													</li>
													<li class="hoverHorarios">
														<a href=""><i class="far fa-clock"></i> Horarios</a>
														<div class="horarios">	
															<strong>Almoço/Jantar</strong>
															<span>Segunda e terça: 11:30h ás 23h</span>
															<span>Quarta e sexta: 11:30h ás 00h</span>
															<span>Sábado: 12h à 00h</span>
															<span>Domingo: 12h ás 22h</span>
														</div>
													</li>
													<li>
														<a href=""><i class="far fa-envelope"></i> E-mail</a>
													</li>		
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
		</footer>
	<div class="copyrightBlog">
		<p>Feito com <i class="fas fa-heart"></i> pelo Comandante.</p>
	</div>

		<script src="<?php bloginfo('template_directory'); ?>/blog/carrossel/slick/slick.js" type="text/javascript" charset="utf-8"></script>
		<script>
			$(function(){


	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	
	

	$(document).on('ready', function() {

		$(".center").slick({
			centerMode: true,
			centerPadding: '90px',
			slidesToShow: 1,
			speed: 2000,
			dots: true,
			responsive: [
			{
				breakpoint: 1333,
				settings: {
					centerMode: false,
				}
			},
			
			]
		});


	});

	//CARROSSEL DE DESTAQUE
	$("#carrosselCategoriaBlog").owlCarousel({
		items : 4,
        dots: true,
        loop: true,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,
	    responsiveClass:true,
			    responsive:{
			         0:{
			            items:1,
			            
			        },
			        540:{
			            items:2,
			            
			        },
			        768:{
			            items:3,
			            
			        },
			        1190:{
			            items:4,
			            
			        }
			    }		  		
	    
	});
	
	
		
});
		</script>
<?php

//get_footer();
 
 