<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'victorest_site');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'victorest_root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'r@pp3965');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1.@D0JQh#Pn0RJirk:,UGYY?ry^ ^(7TtT9<HE:U1a20DQBPrG30-C67b,d-Pu2K');
define('SECURE_AUTH_KEY',  'k5g:97{fw%MfG;<k!CB9mU$60?Z|DRONo?675NRN*;7K!x)P6EAIx$}!l`K#MEut');
define('LOGGED_IN_KEY',    '!^QQ>]Db`z5xU$cQ}7?@x[T#}Eu=juMB1 p5 Gl^R$XTiz=`pC#+iU/}>vY?%hZT');
define('NONCE_KEY',        '~1v[;u;8z>w65 FiCGi47n$L*:jpB|0T>3ql@H[aQH9TG,PHDmuq^1V5OMJ|?Dee');
define('AUTH_SALT',        'BlE{9^b8^|>fN,?3(]k]^E)A_-dni uHFxj|dd;(&eh.OTGhc:7v>v73z-+,W0x)');
define('SECURE_AUTH_SALT', '],7y>?$ex?TMHv^=7vn|!t>Z ;t =R?4X[anGbcTfp_>NyAW#Dfb,y5)E@JXF,l&');
define('LOGGED_IN_SALT',   '+HsS38Fwh`ka-+F?}BIynqK4B[=.#,D<tk@cA62yaSB4;^WzS.y&l7Nk@EI%JA8L');
define('NONCE_SALT',       '8FksZ9j|uUOKBQwv+h8PgEjjC64)FX_6H-/F^%;N0WMsnuyEaS8uQ4c$J=k_rU*M');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'rv_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
