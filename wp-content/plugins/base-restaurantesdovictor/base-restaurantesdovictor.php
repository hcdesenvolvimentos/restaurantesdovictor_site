<?php

/**
 * Plugin Name: Base Restaurantes do Victor
 * Description: Controle base do tema Base Restaurantes do Victor.
 * Version: 0.1
 * Author: Agência Palupa
 * Author URI: http://www.palupa.com.br
 * Licence: GPL2
 */


	function baseRestaurantesdovictor () {

		// TIPOS DE CONTEÚDO
		conteudosRestaurantesdovictor();

		// TAXONOMIA
		taxonomiaRestaurantesdovictor();

		// META BOXES
		metaboxesRestaurantesdovictor();

	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosRestaurantesdovictor (){

		// TIPOS DE CONTEÚDO

		tipoPaginaInicial();

		tipoDestaque();
		
		tipoEstabelecimento();

		tipoEvento();		
		
		tipoEquipe();

		tipoClube();

		tipoLinhadotempo();

		tipoCardapio();

		//tipoCardapioBistro();

		tipoCardapioPetiscaria();

		//tipoCardapioBarespanha();


		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
					$titulo = 'Título do destaque';
				break;

				case 'estabelecimento':
					$titulo = 'Nome do estabelecimento';
				break;

				case 'evento':
					$titulo = 'Nome do evento';
				break;				

				case 'integrante':
					$titulo = 'Nome do integrante';
				break;

				case 'clube':
					$titulo = 'Nome';
				break;

				case 'Linhadotempo':
					$titulo = 'Nome';
				break;

				case 'cardapio':
					$titulo = 'Nome do prato';
				break;

				case 'cardapio_bistro':
					$titulo = 'Nome do prato';
				break;

				case 'cardapio_petiscaria':
					$titulo = 'Nome do prato';
				break;

				case 'cardapio_barespanha':
					$titulo = 'Nome do prato';
				break;

				default:
				break;
			}

		    return $titulo;

		}

	}

	// CUSTOM POST TYPE DESTAQUES
	function tipoPaginaInicial() {

		$rotulosDestaqueInicial = array(
								'name'               => 'Destaques inicial',
								'singular_name'      => 'destaque inicial',
								'menu_name'          => 'Destaques inicial',
								'name_admin_bar'     => 'Destaques inicial',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo destaque',
								'new_item'           => 'Novo destaque',
								'edit_item'          => 'Editar destaque',
								'view_item'          => 'Ver destaque',
								'all_items'          => 'Todos os destaques',
								'search_items'       => 'Buscar destaque',
								'parent_item_colon'  => 'Dos destaques',
								'not_found'          => 'Nenhum destaque cadastrado.',
								'not_found_in_trash' => 'Nenhum destaque na lixeira.'
							);

		$argsDestaqueInicial 	= array(
								'labels'             => $rotulosDestaqueInicial,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'destaque-inicial' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail','editor')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('destaque-inicial', $argsDestaqueInicial);

	}

	// CUSTOM POST TYPE DESTAQUES
	function tipoDestaque() {

		$rotulosDestaque = array(
								'name'               => 'Destaques',
								'singular_name'      => 'destaque',
								'menu_name'          => 'Destaques',
								'name_admin_bar'     => 'Destaques',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo destaque',
								'new_item'           => 'Novo destaque',
								'edit_item'          => 'Editar destaque',
								'view_item'          => 'Ver destaque',
								'all_items'          => 'Todos os destaques',
								'search_items'       => 'Buscar destaque',
								'parent_item_colon'  => 'Dos destaques',
								'not_found'          => 'Nenhum destaque cadastrado.',
								'not_found_in_trash' => 'Nenhum destaque na lixeira.'
							);

		$argsDestaque 	= array(
								'labels'             => $rotulosDestaque,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'destaque' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('destaque', $argsDestaque);

	}

	// CUSTOM POST TYPE LOCAIS
	function tipoEstabelecimento() {

		$rotulosEstabelecimento = array(
								'name'               => 'Estabelecimentos',
								'singular_name'      => 'estabelecimento',
								'menu_name'          => 'Estabelecimentos',
								'name_admin_bar'     => 'Estabelecimentos',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo estabelecimento',
								'new_item'           => 'Novo estabelecimento',
								'edit_item'          => 'Editar estabelecimento',
								'view_item'          => 'Ver estabelecimento',
								'all_items'          => 'Todos os estabelecimentos',
								'search_items'       => 'Buscar estabelecimento',
								'parent_item_colon'  => 'Dos estabelecimentos',
								'not_found'          => 'Nenhum estabelecimento cadastrado.',
								'not_found_in_trash' => 'Nenhum estabelecimento na lixeira.'
							);

		$argsEstabelecimento 	= array(
								'labels'             => $rotulosEstabelecimento,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-admin-multisite',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'estabelecimento' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title', 'thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('estabelecimento', $argsEstabelecimento);

	}

	// CUSTOM POST TYPE EVENTO
	function tipoEvento() {

		$rotulosEvento = array(
								'name'               => 'Eventos',
								'singular_name'      => 'evento',
								'menu_name'          => 'Eventos',
								'name_admin_bar'     => 'Eventos',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo evento',
								'new_item'           => 'Novo evento',
								'edit_item'          => 'Editar evento',
								'view_item'          => 'Ver evento',
								'all_items'          => 'Todos os eventos',
								'search_items'       => 'Buscar evento',
								'parent_item_colon'  => 'Dos eventos',
								'not_found'          => 'Nenhum evento cadastrado.',
								'not_found_in_trash' => 'Nenhum evento na lixeira.'
							);

		$argsEvento 	= array(
								'labels'             => $rotulosEvento,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-pressthis',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'eventos' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title', 'thumbnail','editor')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('eventos', $argsEvento);

	}
	
	// CUSTOM POST TYPE EQUIPE
	function tipoEquipe() {

		$rotulosEquipe = array(
								'name'               => 'Integrantes',
								'singular_name'      => 'integrante',
								'menu_name'          => 'Equipe',
								'name_admin_bar'     => 'integrantes',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo integrante',
								'new_item'           => 'Novo integrante',
								'edit_item'          => 'Editar integrante',
								'view_item'          => 'Ver integrante',
								'all_items'          => 'Todos os integrantes',
								'search_items'       => 'Buscar integrante',
								'parent_item_colon'  => 'Dos integrantes',
								'not_found'          => 'Nenhum integrante cadastrado.',
								'not_found_in_trash' => 'Nenhum integrante na lixeira.'
							);

		$argsEquipe	= array(
								'labels'             => $rotulosEquipe,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-groups',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'equipe' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title', 'thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('equipe', $argsEquipe);

	}

	// CUSTOM POST TYPE CLUBE
	function tipoClube() {

		$rotulosClube = array(
								'name'               => 'Post',
								'singular_name'      => 'post',
								'menu_name'          => 'Clube',
								'name_admin_bar'     => 'post',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo post',
								'new_item'           => 'Novo post',
								'edit_item'          => 'Editar post',
								'view_item'          => 'Ver post',
								'all_items'          => 'Todos os posts',
								'search_items'       => 'Buscar post',
								'parent_item_colon'  => 'Dos posts',
								'not_found'          => 'Nenhum post cadastrado.',
								'not_found_in_trash' => 'Nenhum post na lixeira.'
							);

		$argsClube	= array(
								'labels'             => $rotulosClube,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-universal-access-alt',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'clube' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title', 'thumbnail','editor')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('clube', $argsClube);

	}

	// CUSTOM POST TYPE CLUBE
	function tipoLinhadotempo() {

		$rotulosLinhadotempo = array(
								'name'               => 'Post',
								'singular_name'      => 'post',
								'menu_name'          => 'Linha do tempo',
								'name_admin_bar'     => 'post',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo post',
								'new_item'           => 'Novo post',
								'edit_item'          => 'Editar post',
								'view_item'          => 'Ver post',
								'all_items'          => 'Todos os posts',
								'search_items'       => 'Buscar post',
								'parent_item_colon'  => 'Dos posts',
								'not_found'          => 'Nenhum post cadastrado.',
								'not_found_in_trash' => 'Nenhum post na lixeira.'
							);

		$argsLinhadotempo	= array(
								'labels'             => $rotulosLinhadotempo,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-editor-ul',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'linhadotempo' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('linhadotempo', $argsLinhadotempo);

	}

	// CUSTOM POST TYPE CARDÁPIO BAR DO VICTOR 
	function tipoCardapio() {

		$rotulosCardapio = array(
								'name'               => 'Pratos',
								'singular_name'      => 'prato',
								'menu_name'          => 'Cardápio Principal',
								'name_admin_bar'     => 'Pratos',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo prato',
								'new_item'           => 'Novo prato',
								'edit_item'          => 'Editar prato',
								'view_item'          => 'Ver prato',
								'all_items'          => 'Todos os pratos',
								'search_items'       => 'Buscar prato',
								'parent_item_colon'  => 'Dos pratos',
								'not_found'          => 'Nenhum prato cadastrado.',
								'not_found_in_trash' => 'Nenhum prato na lixeira.'
							);

		$argsCardapio	= array(
								'labels'             => $rotulosCardapio,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-store',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'cardapio' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title', 'thumbnail','editor')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('cardapio', $argsCardapio);

	}

	// CUSTOM POST TYPE CARDÁPIO BISTRÔ DO VICTOR 
	// function tipoCardapioBistro() {

	// 	$rotulosCardapioBistro = array(
	// 							'name'               => 'Pratos',
	// 							'singular_name'      => 'prato',
	// 							'menu_name'          => 'Cardápio Bistrô ',
	// 							'name_admin_bar'     => 'Pratos',
	// 							'add_new'            => 'Adicionar novo',
	// 							'add_new_item'       => 'Adicionar novo prato',
	// 							'new_item'           => 'Novo prato',
	// 							'edit_item'          => 'Editar prato',
	// 							'view_item'          => 'Ver prato',
	// 							'all_items'          => 'Todos os pratos',
	// 							'search_items'       => 'Buscar prato',
	// 							'parent_item_colon'  => 'Dos pratos',
	// 							'not_found'          => 'Nenhum prato cadastrado.',
	// 							'not_found_in_trash' => 'Nenhum prato na lixeira.'
	// 						);

	// 	$argsCardapioBistro	= array(
	// 							'labels'             => $rotulosCardapioBistro,
	// 							'public'             => true,
	// 							'publicly_queryable' => true,
	// 							'show_ui'            => true,
	// 							'show_in_menu'       => true,
	// 							'menu_position'		 => 4,
	// 							'menu_icon'          => 'dashicons-store',
	// 							'query_var'          => true,
	// 							'rewrite'            => array( 'slug' => 'cardapio-bistro' ),
	// 							'capability_type'    => 'post',
	// 							'has_archive'        => true,
	// 							'hierarchical'       => false,
	// 							'supports'           => array( 'title', 'thumbnail','editor')
	// 						);

	// 	// REGISTRA O TIPO CUSTOMIZADO
	// 	register_post_type('cardapio_bistro', $argsCardapioBistro);

	// }

	// CUSTOM POST TYPE CARDÁPIO PETISCARIA DO VICTOR 
	function tipoCardapioPetiscaria() {

		$rotulosCardapioPetiscaria = array(
								'name'               => 'Pratos',
								'singular_name'      => 'prato',
								'menu_name'          => 'Cardápio Petiscaria',
								'name_admin_bar'     => 'Pratos',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo prato',
								'new_item'           => 'Novo prato',
								'edit_item'          => 'Editar prato',
								'view_item'          => 'Ver prato',
								'all_items'          => 'Todos os pratos',
								'search_items'       => 'Buscar prato',
								'parent_item_colon'  => 'Dos pratos',
								'not_found'          => 'Nenhum prato cadastrado.',
								'not_found_in_trash' => 'Nenhum prato na lixeira.'
							);

		$argsCardapioPetiscaria	= array(
								'labels'             => $rotulosCardapioPetiscaria,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-store',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'cardapio-petiscaria' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title', 'thumbnail','editor')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('cardapio_petiscaria', $argsCardapioPetiscaria);

	}

	// CUSTOM POST TYPE CARDÁPIO BAR DO VICTOR PRAÇA DA ESPANHA 
	// function tipoCardapioBarespanha() {

	// 	$rotulosCardapioBarespanha = array(
	// 							'name'               => 'Pratos',
	// 							'singular_name'      => 'prato',
	// 							'menu_name'          => 'Cardápio Bar Praça da Espanha',
	// 							'name_admin_bar'     => 'Pratos',
	// 							'add_new'            => 'Adicionar novo',
	// 							'add_new_item'       => 'Adicionar novo prato',
	// 							'new_item'           => 'Novo prato',
	// 							'edit_item'          => 'Editar prato',
	// 							'view_item'          => 'Ver prato',
	// 							'all_items'          => 'Todos os pratos',
	// 							'search_items'       => 'Buscar prato',
	// 							'parent_item_colon'  => 'Dos pratos',
	// 							'not_found'          => 'Nenhum prato cadastrado.',
	// 							'not_found_in_trash' => 'Nenhum prato na lixeira.'
	// 						);

	// 	$argsCardapioBarespanha	= array(
	// 							'labels'             => $rotulosCardapioBarespanha,
	// 							'public'             => true,
	// 							'publicly_queryable' => true,
	// 							'show_ui'            => true,
	// 							'show_in_menu'       => true,
	// 							'menu_position'		 => 4,
	// 							'menu_icon'          => 'dashicons-store',
	// 							'query_var'          => true,
	// 							'rewrite'            => array( 'slug' => 'cardapio-bar-praca-espanha' ),
	// 							'capability_type'    => 'post',
	// 							'has_archive'        => true,
	// 							'hierarchical'       => false,
	// 							'supports'           => array( 'title', 'thumbnail','editor')
	// 						);

	// 	// REGISTRA O TIPO CUSTOMIZADO
	// 	register_post_type('cardapio_barespanha', $argsCardapioBarespanha);

	// }

	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaRestaurantesdovictor () {		
		
		taxonomiaCategoriaDestaque();

		taxonomiaCategoriaCardapio();

		taxonomiaCategoriaCardapioBistro();

		taxonomiaCategoriaCardapioPetiscaria();

		taxonomiaCategoriaCardapioBarespanha();

		taxonomiaCategoriaEstabelecimento();

		taxonomiaCategoriaEvento();

		taxonomiaCategoriaEquipe();

		taxonomiaCategoriaLinhatmepo();

	}
		// TAXONOMIA DE DESTAQUE
		function taxonomiaCategoriaDestaque() {

			$rotulosCategoriaDestaque = array(
				'name'              => 'Categorias de destaque',
				'singular_name'     => 'Categoria de destaque',
				'search_items'      => 'Buscar categorias de destaque',
				'all_items'         => 'Todas as categorias de destaque',
				'parent_item'       => 'Categoria de destaque pai',
				'parent_item_colon' => 'Categoria de destaque pai:',
				'edit_item'         => 'Editar categoria de destaque',
				'update_item'       => 'Atualizar categoria de destaque',
				'add_new_item'      => 'Nova categoria de destaque',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias de destaque',
				);

			$argsCategoriaDestaque 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaDestaque,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-destaque' ),
				);

			register_taxonomy( 'categoriaDestaque', array( 'destaque' ), $argsCategoriaDestaque );

		}			
		// TAXONOMIA DE ESTABELICIEMNTO
		function taxonomiaCategoriaEstabelecimento() {

			$rotulosCategoriaEstabelecimento = array(
				'name'              => 'Categorias de estabelecimento',
				'singular_name'     => 'Categoria de estabelecimento',
				'search_items'      => 'Buscar categorias de estabelecimento',
				'all_items'         => 'Todas as categorias de estabelecimento',
				'parent_item'       => 'Categoria de estabelecimento pai',
				'parent_item_colon' => 'Categoria de estabelecimento pai:',
				'edit_item'         => 'Editar categoria de estabelecimento',
				'update_item'       => 'Atualizar categoria de estabelecimento',
				'add_new_item'      => 'Nova categoria de estabelecimento',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias de estabelecimento',
				);

			$argsCategoriaEstabelecimento 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaEstabelecimento,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-estabelecimento' ),
				);

			register_taxonomy( 'categoriaEstabelecimento', array( 'estabelecimento' ), $argsCategoriaEstabelecimento );

		}
		// TAXONOMIA DE EVENTOS
		function taxonomiaCategoriaEvento() {

			$rotulosCategoriaEvento = array(
				'name'              => 'Categorias de evento',
				'singular_name'     => 'Categoria de evento',
				'search_items'      => 'Buscar categorias de evento',
				'all_items'         => 'Todas as categorias de evento',
				'parent_item'       => 'Categoria de evento pai',
				'parent_item_colon' => 'Categoria de evento pai:',
				'edit_item'         => 'Editar categoria de evento',
				'update_item'       => 'Atualizar categoria de evento',
				'add_new_item'      => 'Nova categoria de evento',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias de evento',
				);

			$argsCategoriaEvento 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaEvento,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-evento' ),
				);

			register_taxonomy( 'categoriaEvento', array( 'eventos' ), $argsCategoriaEvento );

		}
		// TAXONOMIA DE EQUIPE
		function taxonomiaCategoriaEquipe() {

			$rotulosCategoriaEquipe = array(
				'name'              => 'Categorias de equipe',
				'singular_name'     => 'Categoria de equipe',
				'search_items'      => 'Buscar categorias de equipe',
				'all_items'         => 'Todas as categorias de equipe',
				'parent_item'       => 'Categoria de equipe pai',
				'parent_item_colon' => 'Categoria de equipe pai:',
				'edit_item'         => 'Editar categoria de equipe',
				'update_item'       => 'Atualizar categoria de equipe',
				'add_new_item'      => 'Nova categoria de equipe',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias de equipe',
				);

			$argsCategoriaEquipe 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaEquipe,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-equipe' ),
				);

			register_taxonomy( 'categoriaEquipe', array( 'equipe' ), $argsCategoriaEquipe );

		}

		// TAXONOMIA DE LINHA DO TEMPO
		function taxonomiaCategoriaLinhatmepo() {

			$rotulosCategoriaLinhatmepo = array(
				'name'              => 'Categorias da linha do tempo',
				'singular_name'     => 'Categoria da linha do tempo',
				'search_items'      => 'Buscar categorias das linha do tempo',
				'all_items'         => 'Todas as categorias da linha do tempo',
				'parent_item'       => 'Categoria pai',
				'parent_item_colon' => 'Categoria  pai:',
				'edit_item'         => 'Editar categoria da linha do tempo',
				'update_item'       => 'Atualizar categoria da linha do tempo',
				'add_new_item'      => 'Nova categoria da linha do tempo',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias da linha do tempo',
				);

			$argsCategoriaLinhatmepo 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaLinhatmepo,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'Linha-do-tempo' ),
				);

			register_taxonomy( 'categorialinhadotempo', array( 'linhadotempo' ), $argsCategoriaLinhatmepo );

		}

		/****************************************************
		* TAXONOMIA CARDÁPIO
		*****************************************************/
		
		// TAXONOMIA BAR DO VICTOR SÂO LOURENÇO
		function taxonomiaCategoriaCardapio() {

			$rotulosCategoriaCardapio = array(
				'name'              => 'Categorias do cardápio',
				'singular_name'     => 'Categoria do cardápio',
				'search_items'      => 'Buscar categorias do cardápio',
				'all_items'         => 'Todas as categorias do cardápio',
				'parent_item'       => 'Categoria do cardápio pai',
				'parent_item_colon' => 'Categoria do cardápio pai:',
				'edit_item'         => 'Editar categoria do cardápio',
				'update_item'       => 'Atualizar categoria do cardápio',
				'add_new_item'      => 'Nova categoria do cardápio',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias do cardápio',
				);

			$argsCategoriaCardapio 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaCardapio,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-cardapio' ),
				);

			register_taxonomy( 'categoriaCardapio', array( 'cardapio' ), $argsCategoriaCardapio );

		}

		// TAXONOMIA BISTRÔ DO VICTOR 
		function taxonomiaCategoriaCardapioBistro() {

			$rotulosCategoriaCardapioBistro = array(
				'name'              => 'Categorias do cardápio',
				'singular_name'     => 'Categoria do cardápio',
				'search_items'      => 'Buscar categorias do cardápio',
				'all_items'         => 'Todas as categorias do cardápio',
				'parent_item'       => 'Categoria do cardápio pai',
				'parent_item_colon' => 'Categoria do cardápio pai:',
				'edit_item'         => 'Editar categoria do cardápio',
				'update_item'       => 'Atualizar categoria do cardápio',
				'add_new_item'      => 'Nova categoria do cardápio',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias do cardápio',
				);

			$argsCategoriaCardapioBistro 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaCardapioBistro,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-bistro' ),
				);

			 register_taxonomy( 'categoriaBistro', array( 'cardapio_bistro' ), $argsCategoriaCardapioBistro );

		}

		// TAXONOMIA PETISCARIA DO VICTOR 
		function taxonomiaCategoriaCardapioPetiscaria() {

			$rotulosCategoriaCardapioPetiscaria = array(
				'name'              => 'Categorias do cardápio',
				'singular_name'     => 'Categoria do cardápio',
				'search_items'      => 'Buscar categorias do cardápio',
				'all_items'         => 'Todas as categorias do cardápio',
				'parent_item'       => 'Categoria do cardápio pai',
				'parent_item_colon' => 'Categoria do cardápio pai:',
				'edit_item'         => 'Editar categoria do cardápio',
				'update_item'       => 'Atualizar categoria do cardápio',
				'add_new_item'      => 'Nova categoria do cardápio',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias do cardápio',
				);

			$argsCategoriaCardapioPetiscaria 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaCardapioPetiscaria,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-petiscaria' ),
				);

			register_taxonomy( 'categoriaPetiscaria', array( 'cardapio_petiscaria' ), $argsCategoriaCardapioPetiscaria );

		}

		// TAXONOMIA  BAR DO VICTOR PRAÇA DA ESPANHA 
		function taxonomiaCategoriaCardapioBarespanha() {

			$rotulosCategoriaCardapioBarespanha = array(
				'name'              => 'Categorias do cardápio',
				'singular_name'     => 'Categoria do cardápio',
				'search_items'      => 'Buscar categorias do cardápio',
				'all_items'         => 'Todas as categorias do cardápio',
				'parent_item'       => 'Categoria do cardápio pai',
				'parent_item_colon' => 'Categoria do cardápio pai:',
				'edit_item'         => 'Editar categoria do cardápio',
				'update_item'       => 'Atualizar categoria do cardápio',
				'add_new_item'      => 'Nova categoria do cardápio',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias do cardápio',
				);

			$argsCategoriaCardapioBarespanha 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaCardapioBarespanha,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'cardapio-bar' ),
				);

			register_taxonomy( 'categoriaBarespanha', array( 'cardapio_barespanha' ), $argsCategoriaCardapioBarespanha );

		}


    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesRestaurantesdovictor(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'Restaurantesdovictor_';

			//POST
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxPost',
				'title'			=> 'Detalhes do post',
				'pages' 		=> array( 'post' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					
				
					array(
						'name'  => 'Destaque:',
						'id'    => "{$prefix}post_destaque",
						'desc'  => 'Destaque',
						'type'  => 'checkbox',						
					),
				
				),
			);
			// METABOX DE DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxDestaqueInicial',
				'title'			=> 'Detalhes do Destaque',
				'pages' 		=> array( 'destaque-inicial' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Foto Mobile: ',
						'id'    => "{$prefix}foto_mob",
						'desc'  => 'Foto mobile 320 X 480',
						'type'  => 'image_advanced',
						'max_file_uploads' => 1	
					),	
					
					array(
						'name'  => 'Vídeo: ',
						'id'    => "{$prefix}checkbox_destaqueinicial",
						'desc'  => 'Destaque do tipo vídeo marque aqui!',
						'type'  => 'checkbox'
					),
					array(
						'name'  => 'Imagem: ',
						'id'    => "{$prefix}checkbox_imagem",
						'desc'  => 'Marque está opção, se for arte pronta',
						'type'  => 'checkbox'
					),
					array(
						'name'  => 'Vídeo URL: ',
						'id'    => "{$prefix}video_destaqueinicial",
						'desc'  => 'Adicione o vídeo as mídias e adicione a url aqui!',
						'type'  => 'text'
					),	
									
				),
			);

			// METABOX DE DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxDestaque',
				'title'			=> 'Detalhes do Destaque',
				'pages' 		=> array( 'destaque' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					
					
					array(
						'name'  => 'Vídeo: ',
						'id'    => "{$prefix}checkbox_destaque",
						'desc'  => 'Destaque do tipo vídeo marque aqui!',
						'type'  => 'checkbox'
					),
					array(
						'name'  => 'Vídeo URL: ',
						'id'    => "{$prefix}video_destaque",
						'desc'  => 'Adicione o vídeo as mídias e adicione a url aqui!',
						'type'  => 'text'
					),	
					array(
						'name'  => 'Link post: ',
						'id'    => "{$prefix}link_post",
						'desc'  => 'Link banner',
						'type'  => 'text'
					),					
				),
			);
			
			// METABOX DE LOCAIS
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxLocais',
				'title'			=> 'Detalhes do local',
				'pages' 		=> array( 'estabelecimento' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Logo do site: ',
						'id'    => "{$prefix}logo_estabelecimento",
						'desc'  => '',
						'type'  => 'image',
						'max_file_uploads'  => 1,
						
					),
					
					array(
						'name'  => 'Link para o site: ',
						'id'    => "{$prefix}link_estabelecimento",
						'desc'  => '',
						'type'  => 'text',
						'placeholder'  => 'www.bistro do Victor.com.br',
					),

					array(
						'name'  => 'Local: ',
						'id'    => "{$prefix}local_estabelecimento",
						'desc'  => '',
						'type'  => 'text',
						'placeholder'  => 'Park Shopping Barigui',
					),
					array(
						'name'  => 'Endereço: ',
						'id'    => "{$prefix}endereco_estabelecimento",
						'desc'  => '',
						'type'  => 'text',
						'placeholder'  => 'Rua Exemplo ,000 Curitiba-PR',
					),
					array(
						'name'  => 'Telefone: ',
						'id'    => "{$prefix}telefone_estabelecimento",
						'desc'  => '',
						'type'  => 'text',
						'placeholder'  => 'Telefone',
					),
					array(
						'name'  => 'Horário de funcionamento Almoço: ',
						'id'    => "{$prefix}horarioFuncionamento_estabelecimentoA",
						'desc'  => 'Separar por | dias do horário',
						'clone'  => true,
						'type'  => 'text',
						'max_clone'  => 5,
						'placeholder'  => 'Segunda e terças das | 12 às 23h',
					),
					array(
						'name'  => 'Horário de funcionamento Janta: ',
						'id'    => "{$prefix}horarioFuncionamento_estabelecimentoJ",
						'desc'  => 'Separar por | dias do horário',
						'clone'  => true,
						'type'  => 'text',
						'max_clone'  => 5,
						'placeholder'  => 'Segunda e terças das | 12 às 23h',
					),
					array(
						'name'  => 'Redes sociais: ',
						'id'    => "{$prefix}facebookestabelecimento",
						'desc'  => 'facebook',
						'type'  => 'text',
						'placeholder'  => 'https://www.facebook.com/',
					),
					array(
						'name'  => 'Redes sociais: ',
						'id'    => "{$prefix}twitter_estabelecimento",
						'desc'  => 'instagram',
						'type'  => 'text',
						'placeholder'  => 'https://instagram.com/',
					),
					array(
						'name'  => 'Link Página: ',
						'id'    => "{$prefix}linkpagina_estabelecimento",
						'desc'  => '',
						'type'  => 'text',
						'placeholder'  => 'URL Página',
					),
					array(
						'name'  => 'Formulário de reserva: ',
						'id'    => "{$prefix}form_reserva",
						'desc'  => '',
						'type'  => 'text',
					
					),

					array(
						'name'  => 'Link cardápio: ',
						'id'    => "{$prefix}link_cardapio",
						'desc'  => '',
						'type'  => 'text',
					
					),

				
				),
			);
			
			//METABOX DE EVENTOS		
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxEventos',
				'title'			=> 'Detalhes do evento',
				'pages' 		=> array( 'eventos' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Subtítulo do evento: ',
						'id'    => "{$prefix}evento_subtitulo",
						'desc'  => '',
						'type'  => 'text'
					),

					array(
						'name'  => 'Data do evento: ',
						'id'    => "{$prefix}evento_data",
						'desc'  => '',
						'type'  => 'text',
						'placeholder'  => '08 de maio de 2016 no Almoço após ás 14:30h',
					),

					array(
						'name'  => 'Pratos do evento: ',
						'id'    => "{$prefix}evento_pratos",
						'desc'  => '',
						'type'  	 => 'post',
						'post_type'  => 'cardapio',
						'field_type' => 'select',
						'multiple' => true
					),

					array(
						'name'  => 'Casas Participantes: ',
						'id'    => "{$prefix}evento_casas",
						'desc'  => '',
						'type'  	 => 'post',
						'post_type'  => 'estabelecimento',
						'field_type' => 'select',
						'multiple' => true
					),

					array(
						'name'  => 'Link para o evento blog: ',
						'id'    => "{$prefix}evento_linkEvento",
						'desc'  => '',
						'type'  	 => 'text',
					),
					array(
						'name'  => 'Imagem pequena: ',
						'id'    => "{$prefix}img_linkEvento",
						'desc'  => '',
						'type'  => 'image',
						'max_file_uploads'  => 1,
					),

				),
			);

			//METABOX DE EQUIPE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxEquipe',
				'title'			=> 'Detalhes do integrante',
				'pages' 		=> array( 'equipe' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					
					
					array(
						'name'  => 'Cargo: ',
						'id'    => "{$prefix}integrante_cargo",
						'desc'  => '',
						'type'  => 'text',
						
					),
					array(
						'name'  => 'Função: ',
						'id'    => "{$prefix}integrante_funcao",
						'desc'  => '',
						'type'  => 'text',
						
					),
					
					
				),
			);
		
			//METABOX DE LINHA DO TEMPO
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxLinhadotempo',
				'title'			=> 'Detalhes da linha do tempo',
				'pages' 		=> array( 'linhadotempo' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					
					
					array(
						'name'  => 'Ano: ',
						'id'    => "{$prefix}LinhadotempoAno",
						'desc'  => '',
						'type'  => 'text',
						
					),	

					array(
						'name'  => 'Descrição: ',
						'id'    => "{$prefix}LinhadotempoDescricao",
						'desc'  => '',
						'type'  => 'textarea',
						
					),			
					
				),
			);
			
	 	/****************************************************
		* META BOXES CARDÁPIO
		*****************************************************/
		
			// METABOX BAR DO VICTOR SÃO LOURENÇO 
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxCardapio',
				'title'			=> 'Detalhes do prato',
				'pages' 		=> array( 'cardapio' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					
				
					array(
						'name'  => 'Vegetariano:',
						'id'    => "{$prefix}cardapio_vegetariano",
						'desc'  => 'Marque se tiver',
						'type'  => 'checkbox',						
					),
					array(
						'name'  => 'Lactose:',
						'id'    => "{$prefix}cardapio_lactose",
						'desc'  => 'Marque se tiver',
						'type'  => 'checkbox',						
					),
					array(
						'name'  => 'Galeria:',
						'id'    => "{$prefix}prato_galeria",
						'desc'  => 'Marque se tiver',
						'type'  => 'checkbox',						
					),
					array(
						'name'  => 'Galeria: ',
						'id'    => "{$prefix}cardapio_galeria",
						'desc'  => '5 fotos',
						'type'  => 'image_advanced',
						'max_file_uploads' => 5	
					),
				
				),
			);

			// METABOX BISTRÔ DO VICTOR 
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxBistro',
				'title'			=> 'Detalhes do prato',
				'pages' 		=> array( 'cardapio_bistro' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					
					array(
						'name'  => 'Preço (opcional):',
						'id'    => "{$prefix}prato_Subtitulo_bistro",
						'desc'  => 'Ex: 20,00',
						'type'  => 'text',						
					),
					
					array(
						'name'  => 'Detalhamento dos itens:',
						'id'    => "{$prefix}prato_detalhe_bistro",
						'desc'  => '',
						'type'  => 'textarea',
						'clone'=> 'true',
						'max_clone' => '30',
						'placeholder' => 'Ex: Stella Artois - R$ 11,00 | A Stella Artois é uma cerveja pilsner lager (de baixa fermentação) premium...',
					),
				
				),
			);

			// METABOX PETISCARIA DO VICTOR 
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxCardapioPetiscaria',
				'title'			=> 'Detalhes do prato',
				'pages' 		=> array( 'cardapio_petiscaria' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					
					array(
						'name'  => 'Preço (opcional):',
						'id'    => "{$prefix}prato_Subtitulo_petiscaria",
						'desc'  => 'Ex: 20,00',
						'type'  => 'text',						
					),
					
					array(
						'name'  => 'Detalhamento dos itens:',
						'id'    => "{$prefix}prato_detalhe_petiscaria",
						'desc'  => '',
						'type'  => 'textarea',
						'clone'=> 'true',
						'max_clone' => '30',
						'placeholder' => 'Ex: Stella Artois - R$ 11,00 | A Stella Artois é uma cerveja pilsner lager (de baixa fermentação) premium...',
					),
				
				),
			);

			// METABOX BAR DO VICTOR PRAÇA DA ESPANHA 
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxCardapioEspanha',
				'title'			=> 'Detalhes do prato',
				'pages' 		=> array( 'cardapio_barespanha' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					
					array(
						'name'  => 'Preço (opcional):',
						'id'    => "{$prefix}prato_Subtitulo_espanha",
						'desc'  => 'Ex: 20,00',
						'type'  => 'text',						
					),
					
					array(
						'name'  => 'Detalhamento dos itens:',
						'id'    => "{$prefix}prato_detalhe_espanha",
						'desc'  => '',
						'type'  => 'textarea',
						'clone'=> 'true',
						'max_clone' => '30',
						'placeholder' => 'Ex: Stella Artois - R$ 11,00 | A Stella Artois é uma cerveja pilsner lager (de baixa fermentação) premium...',
					),
				
				),
			);


	 	/****************************************************
		* META BOXES PÁGINAS
		*****************************************************/ 

			//METABOX PÁGINA INICIAL
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxInicial',
				'title'			=> 'Detalhes da página inicial',
				'pages' 		=> array( 'page' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					
					
					array(
						'name'  => 'Participe dos eventos: ',
						'id'    => "{$prefix}inicial_eventos",
						'desc'  => 'Foto',
						'type'  => 'image',
						'max_file_uploads'  => 1,
						
					),		
					array(
						'name'  => 'Faça sua  reserva: ',
						'id'    => "{$prefix}inicial_reserva",
						'desc'  => 'Foto',
						'type'  => 'image',
						'max_file_uploads'  => 1,
						
					),	
					array(
						'name'  => 'Formulário faça sua  reserva: ',
						'id'    => "{$prefix}inicial_reserva_formulario",
						'desc'  => 'cole o shortcode',
						'type'  => 'text',
						
						
					),				
					
				),
			);			

			//METABOX PÁGINA QUEM SOMOS
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxQuemSomos',
				'title'			=> 'Detalhes da página quem somos',
				'pages' 		=> array( 'page' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					
					
					array(
						'name'  => 'Histporia: ',
						'id'    => "{$prefix}quemSomos_historia",
						'desc'  => 'separar título por | do texto',
						'type'  => 'textarea',
						'placeholder'  => 'A HISTÓRIA DO NOSSO GRUPO PIER DO VICTOR | texto',
				
						
					),		
					array(
						'name'  => 'Como surgiu o bar: ',
						'id'    => "{$prefix}quemSomos_comoSurgiu",
						'desc'  => 'separar título por | do texto',
						'type'  => 'textarea',
						'placeholder'  => 'A HISTÓRIA DO NOSSO GRUPO PIER DO VICTOR | texto',
						
					),	

					array(
						'name'  => 'Imagem topo: ',
						'id'    => "{$prefix}quemSomos_banner",
						'desc'  => 'Foto',
						'type'  => 'image',
						'max_file_uploads'  => 1,
						
					),	
					array(
						'name'  => 'Vídeo: ',
						'id'    => "{$prefix}quemSomos_video_banner",
						'desc'  => 'ser for Vídeo marque a caixa a baixo',
						'type'  => 'textarea',
						'placeholder'  => "Adicione o vídeo as mídias e cole a URL do vídeo aqui!",
						
					),	
					array(
						'name'  => 'Confirme o vídeo : ',
						'id'    => "{$prefix}checkbox_quemsomos",
						'desc'  => '',
						'type'  => 'checkbox'
					),			
					
				),
			);

			//METABOX PÁGINA CONTATO
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxContato',
				'title'			=> 'Detalhes da página de contato',
				'pages' 		=> array( 'page' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					
					
					array(
						'name'  => 'Formulário de contato: ',
						'id'    => "{$prefix}contat_formulario",
						'desc'  => 'shortcode contato',
						'type'  => 'text',
						
						
						
					),		
									
					
				),
			);

			//METABOX PÁGINA CONTATO
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxClube',
				'title'			=> 'Detalhes da página do clube',
				'pages' 		=> array( 'page' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					
					
					array(
						'name'  => 'Formulário do clube: ',
						'id'    => "{$prefix}clube_formulario",
						'desc'  => 'shortcode clube',
						'type'  => 'text',
						
						
						
					),		
									
					
				),
			);
			
			return $metaboxes;
		}

		function metaboxjs(){

			global $post;
			$template = get_post_meta($post->ID, '_wp_page_template', true);
			$template = explode('/', $template);
			$template = explode('.', $template[1]);
			$template = $template[0];

			if($template != ''){
				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
			}
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesRestaurantesdovictor(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerRestaurantesdovictor(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseRestaurantesdovictor');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseRestaurantesdovictor();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );